/*
 * Copyright (c) [2020-2021] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include "alias_class.h"
#include "me_rename2preg.h"
#include "mir_builder.h"
#include "me_irmap.h"

// This phase mainly renames the variables to pseudo register.
// Only non-ref-type variables (including parameters) with no alias are
// workd on here.  Remaining variables are left to LPRE phase.  This is
// because for ref-type variables, their stores have to be left intact.

namespace maple {

RegMeExpr *SSARename2Preg::RenameVar(const VarMeExpr *varmeexpr) {
  if (varmeexpr->GetOst()->GetFieldID() != 0) {
    return nullptr;
  }
  const OriginalSt *ost = varmeexpr->GetOst();
  if (ost->GetIndirectLev() != 0) {
    return nullptr;
  }
  const MIRSymbol *mirst = ost->GetMIRSymbol();
  if (mirst->GetAttr(ATTR_localrefvar)) {
    return nullptr;
  }
  if (ost->IsFormal() && varmeexpr->GetPrimType() == PTY_ref) {
    return nullptr;
  }
  if (ost->IsVolatile()) {
    return nullptr;
  }
  if (sym2reg_map.find(ost->GetIndex()) != sym2reg_map.end()) {
    // replaced previously
    auto verit = vstidx2reg_map.find(varmeexpr->GetExprID());
    RegMeExpr *varreg = nullptr;
    if (verit != vstidx2reg_map.end()) {
      varreg = verit->second;
    } else {
      OriginalSt *pregOst = sym2reg_map[ost->GetIndex()];
      varreg = meirmap->CreateRegMeExprVersion(*pregOst);
      (void)vstidx2reg_map.insert(std::make_pair(varmeexpr->GetExprID(), varreg));
    }
    return varreg;
  } else {
    const OriginalSt *origOst = ost;
    if (origOst->GetIndex() >= aliasclass->GetAliasElemCount()) {
      return nullptr;
    }
    if (!mirst->IsLocal() || mirst->GetStorageClass() == kScPstatic || mirst->GetStorageClass() == kScFstatic) {
      return nullptr;
    }
    if (origOst->IsAddressTaken()) {
      return nullptr;
    }
    AliasElem *aliaselem = GetAliasElem(origOst);
    if (aliaselem && aliaselem->GetClassSet()) {
      return nullptr;
    }
    RegMeExpr *curtemp = nullptr;
    MIRType *ty = GlobalTables::GetTypeTable().GetTypeFromTyIdx(mirst->GetTyIdx());
    if (ty->GetKind() != kTypeScalar && ty->GetKind() != kTypePointer) {
      return nullptr;
    }
    curtemp = meirmap->CreateRegMeExpr(*ty);
    OriginalSt *pregOst =
        ssaTab->GetOriginalStTable().CreatePregOriginalSt(curtemp->GetRegIdx(), func->GetMirFunc()->GetPuidx());
    pregOst->SetIsFormal(ost->IsFormal());
    sym2reg_map[ost->GetIndex()] = pregOst;
    (void)vstidx2reg_map.insert(std::make_pair(varmeexpr->GetExprID(), curtemp));
    if (ost->IsFormal()) {
      uint32 parmindex = func->GetMirFunc()->GetFormalIndex(mirst);
      CHECK_FATAL(parm_used_vec[parmindex], "parm_used_vec not set correctly");
      if (!reg_formal_vec[parmindex]) {
        reg_formal_vec[parmindex] = curtemp;
      }
    }
    if (DEBUGFUNC(func)) {
      ost->Dump();
      LogInfo::MapleLogger() << "(ost idx " << ost->GetIndex() << ") renamed to ";
      pregOst->Dump();
      LogInfo::MapleLogger() << std::endl;
    }
    return curtemp;
  }
}

void SSARename2Preg::Rename2PregCallReturn(MapleVector<MustDefMeNode> &mustdeflist) {
  if (mustdeflist.empty()) {
    return;
  }
  CHECK_FATAL(mustdeflist.size() == 1, "NYI");
  {
    MustDefMeNode &mustdefmenode = mustdeflist.front();
    MeExpr *melhs = mustdefmenode.GetLHS();
    if (melhs->GetMeOp() != kMeOpVar) {
      return;
    }
    VarMeExpr *lhs = static_cast<VarMeExpr *>(melhs);
    SetupParmUsed(lhs);

    RegMeExpr *varreg = RenameVar(lhs);
    if (varreg != nullptr) {
      mustdefmenode.UpdateLHS(*varreg);
    }
  }
}

// update regphinode operands
void SSARename2Preg::UpdateRegPhi(MePhiNode *mevarphinode, MePhiNode *regphinode, const RegMeExpr *curtemp,
                                  const VarMeExpr *lhs) {
  // update phi's opnds
  for (uint32 i = 0; i < mevarphinode->GetOpnds().size(); i++) {
    ScalarMeExpr *opndexpr = mevarphinode->GetOpnds()[i];
    ASSERT(opndexpr->GetOst()->GetIndex() == lhs->GetOst()->GetIndex(), "phi is not correct");
    auto verit = vstidx2reg_map.find(opndexpr->GetExprID());
    RegMeExpr *opndtemp = nullptr;
    if (verit == vstidx2reg_map.end()) {
      opndtemp = meirmap->CreateRegMeExprVersion(*curtemp);
      (void)vstidx2reg_map.insert(std::make_pair(opndexpr->GetExprID(), opndtemp));
    } else {
      opndtemp = verit->second;
    }
    regphinode->GetOpnds().push_back(opndtemp);
  }
  (void)lhs;
}

void SSARename2Preg::Rename2PregPhi(MePhiNode *mevarphinode, MapleMap<OStIdx, MePhiNode *> &regPhiList) {
  VarMeExpr *lhs = static_cast<VarMeExpr*>(mevarphinode->GetLHS());
  SetupParmUsed(lhs);
  RegMeExpr *lhsreg = RenameVar(lhs);
  if (lhsreg != nullptr) {
    MePhiNode *regphinode = meirmap->CreateMePhi(*lhsreg);
    regphinode->SetDefBB(mevarphinode->GetDefBB());
    UpdateRegPhi(mevarphinode, regphinode, lhsreg, lhs);
    (void)regPhiList.insert(std::make_pair(lhsreg->GetOst()->GetIndex(), regphinode));
  }
}

void SSARename2Preg::Rename2PregLeafRHS(MeStmt *mestmt, const VarMeExpr *varmeexpr) {
  SetupParmUsed(varmeexpr);
  RegMeExpr *varreg = RenameVar(varmeexpr);
  if (varreg != nullptr) {
    (void)meirmap->ReplaceMeExprStmt(*mestmt, *varmeexpr, *varreg);
  }
}

void SSARename2Preg::Rename2PregLeafLHS(MeStmt *mestmt, const VarMeExpr *varmeexpr) {
  SetupParmUsed(varmeexpr);
  RegMeExpr *varreg = RenameVar(varmeexpr);
  if (varreg != nullptr) {
    Opcode desop = mestmt->GetOp();
    CHECK_FATAL(desop == OP_dassign || desop == OP_maydassign, "NYI");
    MeExpr *oldrhs = (desop == OP_dassign) ? (static_cast<DassignMeStmt *>(mestmt)->GetRHS())
                                           : (static_cast<MaydassignMeStmt *>(mestmt)->GetRHS());
    if (GetPrimTypeSize(oldrhs->GetPrimType()) > GetPrimTypeSize(varreg->GetPrimType())) {
      // insert integer truncation
      if (GetPrimTypeSize(varreg->GetPrimType()) >= 4) {
        oldrhs = meirmap->CreateMeExprTypeCvt(varreg->GetPrimType(), oldrhs->GetPrimType(), *oldrhs);
      } else {
        Opcode extOp = IsSignedInteger(varreg->GetPrimType()) ? OP_sext : OP_zext;
        PrimType newPrimType = PTY_u32;
        if (IsSignedInteger(varreg->GetPrimType())) {
          newPrimType = PTY_i32;
        }
        OpMeExpr opmeexpr(-1, extOp, newPrimType, 1);
        opmeexpr.SetBitsSize(GetPrimTypeSize(varreg->GetPrimType()) * 8);
        opmeexpr.SetOpnd(0, oldrhs);
        oldrhs = meirmap->HashMeExpr(opmeexpr);
      }
    }
    RegassignMeStmt *regssmestmt = meirmap->New<RegassignMeStmt>(varreg, oldrhs);
    regssmestmt->CopyBase(*mestmt);
    mestmt->GetBB()->InsertMeStmtBefore(mestmt, regssmestmt);
    mestmt->GetBB()->RemoveMeStmt(mestmt);
  }
}

void SSARename2Preg::SetupParmUsed(const VarMeExpr *varmeexpr) {
  const OriginalSt *ost = varmeexpr->GetOst();
  if (ost->IsFormal() && ost->IsSymbolOst()) {
    const MIRSymbol *mirst = ost->GetMIRSymbol();
    uint32 index = func->GetMirFunc()->GetFormalIndex(mirst);
    parm_used_vec[index] = true;
  }
}

// only handle the leaf of load, because all other expressions has been done by previous SSAPre
void SSARename2Preg::Rename2PregExpr(MeStmt *mestmt, MeExpr *meexpr) {
  MeExprOp meOp = meexpr->GetMeOp();
  switch (meOp) {
    case kMeOpIvar:
    case kMeOpOp:
    case kMeOpNary: {
      for (uint32 i = 0; i < meexpr->GetNumOpnds(); ++i) {
        Rename2PregExpr(mestmt, meexpr->GetOpnd(i));
      }
      break;
    }
    case kMeOpVar:
      Rename2PregLeafRHS(mestmt, static_cast<VarMeExpr *>(meexpr));
      break;
    case kMeOpAddrof: {
      AddrofMeExpr *addrofx = static_cast<AddrofMeExpr *>(meexpr);
      const OriginalSt *ost = ssaTab->GetOriginalStFromID(addrofx->GetOstIdx());
      if (ost->IsFormal()) {
        const MIRSymbol *mirst = ost->GetMIRSymbol();
        uint32 index = func->GetMirFunc()->GetFormalIndex(mirst);
        parm_used_vec[index] = true;
      }
      break;
    }
    default:
      break;
  }
  return;
}

void SSARename2Preg::Rename2PregStmt(MeStmt *stmt) {
  Opcode op = stmt->GetOp();
  switch (op) {
    case OP_dassign:
    case OP_maydassign: {
      CHECK_FATAL(stmt->GetRHS() && stmt->GetVarLHS(), "null ptr check");
      Rename2PregExpr(stmt, stmt->GetRHS());
      Rename2PregLeafLHS(stmt, static_cast<VarMeExpr *>(stmt->GetVarLHS()));
      break;
    }
    case OP_callassigned:
    case OP_virtualcallassigned:
    case OP_virtualicallassigned:
    case OP_superclasscallassigned:
    case OP_interfacecallassigned:
    case OP_interfaceicallassigned:
    case OP_customcallassigned:
    case OP_polymorphiccallassigned:
    case OP_icallassigned:
    case OP_intrinsiccallassigned:
    case OP_xintrinsiccallassigned:
    case OP_intrinsiccallwithtypeassigned: {
      for (uint32 i = 0; i < stmt->NumMeStmtOpnds(); ++i) {
        Rename2PregExpr(stmt, stmt->GetOpnd(i));
      }
      MapleVector<MustDefMeNode> *mustdeflist = stmt->GetMustDefList();
      Rename2PregCallReturn(*mustdeflist);
      break;
    }
    case OP_iassign: {
      IassignMeStmt *ivarstmt = static_cast<IassignMeStmt *>(stmt);
      Rename2PregExpr(stmt, ivarstmt->GetRHS());
      Rename2PregExpr(stmt, ivarstmt->GetLHS()->GetBase());
      break;
    }
    default:
      for (uint32 i = 0; i < stmt->NumMeStmtOpnds(); ++i) {
        Rename2PregExpr(stmt, stmt->GetOpnd(i));
      }
      break;
  }
}

void SSARename2Preg::UpdateMirFunctionFormal() {
  MIRFunction *mirFunc = func->GetMirFunc();
  const MIRBuilder *mirbuilder = mirModule->GetMIRBuilder();
  for (uint32 i = 0; i < mirFunc->GetFormalDefVec().size(); i++) {
    if (!parm_used_vec[i]) {
      // in this case, the paramter is not used by any statement, promote it
      MIRType *mirType = GlobalTables::GetTypeTable().GetTypeFromTyIdx(mirFunc->GetFormalDefVec()[i].formalTyIdx);
      if (mirType->GetPrimType() != PTY_agg) {
        PregIdx16 regIdx = mirFunc->GetPregTab()->CreatePreg(
            mirType->GetPrimType(), mirType->GetPrimType() == PTY_ref ? mirType : nullptr);
        mirFunc->GetFormalDefVec()[i].formalSym =
            mirbuilder->CreatePregFormalSymbol(mirType->GetTypeIndex(), regIdx, *mirFunc);
      }
    } else {
      RegMeExpr *regformal = reg_formal_vec[i];
      if (regformal) {
        PregIdx regIdx = regformal->GetRegIdx();
        MIRSymbol *oldformalst = mirFunc->GetFormalDefVec()[i].formalSym;
        MIRSymbol *newformalst = mirbuilder->CreatePregFormalSymbol(oldformalst->GetTyIdx(), regIdx, *mirFunc);
        mirFunc->GetFormalDefVec()[i].formalSym = newformalst;
      }
    }
  }
}

void SSARename2Preg::Init() {
  uint32 formalsize = func->GetMirFunc()->GetFormalDefVec().size();
  parm_used_vec.resize(formalsize);
  reg_formal_vec.resize(formalsize);
}

void SSARename2Preg::RunSelf() {
  Init();
  for (BB *mebb : func->GetAllBBs()) {
    if (mebb == nullptr) {
      continue;
    }
    // rename the phi'ss
    if (DEBUGFUNC(func)) {
      LogInfo::MapleLogger() << " working on phi part of BB" << mebb->GetBBId() << std::endl;
    }
    MapleMap<OStIdx, MePhiNode *> &phiList = mebb->GetMePhiList();
    MapleMap<OStIdx, MePhiNode *> regPhiList(func->GetIRMap()->GetIRMapAlloc().Adapter());
    for (std::pair<const OStIdx, MePhiNode *> apair : phiList) {
      if (!apair.second->UseReg()) {
        Rename2PregPhi(apair.second, regPhiList);
      }
    }
    phiList.insert(regPhiList.begin(), regPhiList.end());

    if (DEBUGFUNC(func)) {
      LogInfo::MapleLogger() << " working on stmt part of BB" << mebb->GetBBId() << std::endl;
    }
    for (MeStmt &stmt : mebb->GetMeStmts()) {
      Rename2PregStmt(&stmt);
    }
  }

  UpdateMirFunctionFormal();
}

void SSARename2Preg::PromoteEmptyFunction() {
  Init();
  UpdateMirFunctionFormal();
}

AnalysisResult *MeDoSSARename2Preg::Run(MeFunction *func, MeFuncResultMgr *m, ModuleResultMgr *mrMgr) {
  if (func->GetAllBBs().empty()) {
    return nullptr;
  }
  (void)mrMgr;
  MeIRMap *irMap = static_cast<MeIRMap *>(m->GetAnalysisResult(MeFuncPhase_IRMAPBUILD, func));
  ASSERT(irMap != nullptr, "");

  MemPool *renamemp = memPoolCtrler.NewMemPool(PhaseName().c_str());
  if (func->GetAllBBs().size() == 0) {
    // empty function, we only promote the parameter
    SSARename2Preg emptyrenamer(renamemp, func, nullptr, nullptr);
    emptyrenamer.PromoteEmptyFunction();
    memPoolCtrler.DeleteMemPool(renamemp);
    return nullptr;
  }

  AliasClass *aliasclass = static_cast<AliasClass *>(m->GetAnalysisResult(MeFuncPhase_ALIASCLASS, func));
  ASSERT(aliasclass != nullptr, "");

  SSARename2Preg ssarename2preg(renamemp, func, irMap, aliasclass);
  ssarename2preg.RunSelf();
  if (DEBUGFUNC(func)) {
    irMap->Dump();
  }
  memPoolCtrler.DeleteMemPool(renamemp);

  return nullptr;
}

}  // namespace maple
