/*
 * Copyright (c) [2020-2021] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include "feir_stmt.h"
#include "opcode_info.h"
#include "literalstrname.h"
#include "mir_type.h"
#include "feir_builder.h"
#include "feir_var_reg.h"
#include "feir_var_name.h"
#include "fe_manager.h"
#include "mplfe_env.h"
#include "feir_var_type_scatter.h"
#include "fe_options.h"
#include "bc_util.h"

namespace maple {
std::string GetFEIRNodeKindDescription(FEIRNodeKind kindArg) {
  switch (kindArg) {
#define FEIR_NODE_KIND(kind, description)                            \
    case k##kind: {                                                  \
      return description;                                            \
    }
#include "feir_node_kind.def"
#undef FEIR_NODE_KIND
    default: {
      CHECK_FATAL(false, "Undefined FEIRNodeKind %u", static_cast<uint32>(kindArg));
      return "";
    }
  }
}

// ---------- FEIRStmt ----------
std::list<StmtNode*> FEIRStmt::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  return std::list<StmtNode*>();
}

bool FEIRStmt::IsStmtInstImpl() const {
  switch (kind) {
    case kStmtAssign:
    case kStmtNonAssign:
    case kStmtDAssign:
    case kStmtJavaTypeCheck:
    case kStmtJavaConstClass:
    case kStmtJavaConstString:
    case kStmtJavaMultiANewArray:
    case kStmtCallAssign:
    case kStmtJavaDynamicCallAssign:
    case kStmtIAssign:
    case kStmtUseOnly:
    case kStmtReturn:
    case kStmtBranch:
    case kStmtGoto:
    case kStmtCondGoto:
    case kStmtSwitch:
    case kStmtArrayStore:
    case kStmtFieldStore:
    case kStmtFieldLoad:
      return true;
    default:
      return false;
  }
}

bool FEIRStmt::IsStmtInstComment() const {
  return (kind == kStmtPesudoComment);
}

bool FEIRStmt::ShouldHaveLOC() const {
  return (IsStmtInstImpl() || IsStmtInstComment());
}

std::string FEIRStmt::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  return ss.str();
}

// ---------- FEIRStmtCheckPoint ----------
void FEIRStmtCheckPoint::Reset() {
  predCPs.clear();
  localUD.clear();
  lastDef.clear();
  cacheUD.clear();
  defs.clear();
  uses.clear();
}

void FEIRStmtCheckPoint::RegisterDFGNode(UniqueFEIRVar &var) {
  CHECK_NULL_FATAL(var);
  if (var->IsDef()) {
    defs.push_back(&var);
    lastDef[FEIRDFGNode(var)] = &var;
  } else {
    uses.push_back(&var);
    auto it = lastDef.find(FEIRDFGNode(var));
    if (it != lastDef.end()) {
      CHECK_FATAL(localUD[&var].insert(it->second).second, "localUD insert failed");
    }
  }
}

void FEIRStmtCheckPoint::RegisterDFGNodes(const std::list<UniqueFEIRVar*> &vars) {
  for (UniqueFEIRVar *var : vars) {
    CHECK_NULL_FATAL(var);
    RegisterDFGNode(*var);
  }
}

void FEIRStmtCheckPoint::RegisterDFGNodeFromAllVisibleStmts() {
  if (firstVisibleStmt == nullptr) {
    return;
  }
  FELinkListNode *node = static_cast<FELinkListNode*>(firstVisibleStmt);
  while (node != this) {
    FEIRStmt *stmt = static_cast<FEIRStmt*>(node);
    stmt->RegisterDFGNodes2CheckPoint(*this);
    node = node->GetNext();
  }
}

void FEIRStmtCheckPoint::AddPredCheckPoint(FEIRStmtCheckPoint &stmtCheckPoint) {
  if (predCPs.find(&stmtCheckPoint) == predCPs.end()) {
    CHECK_FATAL(predCPs.insert(&stmtCheckPoint).second, "pred checkpoints insert error");
  }
}

std::set<UniqueFEIRVar*> &FEIRStmtCheckPoint::CalcuDef(UniqueFEIRVar &use) {
  CHECK_NULL_FATAL(use);
  auto itLocal = localUD.find(&use);
  // search localUD
  if (itLocal != localUD.end()) {
    return itLocal->second;
  }
  // search cacheUD
  auto itCache = cacheUD.find(FEIRDFGNode(use));
  if (itCache != cacheUD.end()) {
    return itCache->second;
  }
  // search by DFS
  std::set<const FEIRStmtCheckPoint*> visitSet;
  std::set<UniqueFEIRVar*> &result = cacheUD[FEIRDFGNode(use)];
  CalcuDefDFS(result, use, *this, visitSet);
  if (result.size() == 0) {
    WARN(kLncWarn, "use var %s without def", use->GetNameRaw().c_str());
  }
  return result;
}

void FEIRStmtCheckPoint::CalcuDefDFS(std::set<UniqueFEIRVar*> &result, const UniqueFEIRVar &use,
                                     const FEIRStmtCheckPoint &cp,
                                     std::set<const FEIRStmtCheckPoint*> &visitSet) const {
  CHECK_NULL_FATAL(use);
  if (visitSet.find(&cp) != visitSet.end()) {
    return;
  }
  CHECK_FATAL(visitSet.insert(&cp).second, "visitSet insert failed");
  auto itLast = cp.lastDef.find(FEIRDFGNode(use));
  if (itLast != cp.lastDef.end()) {
    CHECK_FATAL(result.insert(itLast->second).second, "def insert failed");
    return;
  }
  // optimization by cacheUD
  auto itCache = cp.cacheUD.find(FEIRDFGNode(use));
  if (itCache != cp.cacheUD.end()) {
    for (UniqueFEIRVar *def : itCache->second) {
      CHECK_FATAL(result.insert(def).second, "def insert failed");
    }
    if (itCache->second.size() > 0) {
      return;
    }
  }
  // optimization by cacheUD (end)
  for (const FEIRStmtCheckPoint *pred : cp.predCPs) {
    CHECK_NULL_FATAL(pred);
    CalcuDefDFS(result, use, *pred, visitSet);
  }
}

std::string FEIRStmtCheckPoint::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind) << " preds: [ ";
  for (FEIRStmtCheckPoint *pred : predCPs) {
    ss << pred->GetID() << ", ";
  }
  ss << " ]";
  return ss.str();
}

// ---------- FEIRStmtNary ----------
FEIRStmtNary::FEIRStmtNary(Opcode opIn, std::list<std::unique_ptr<FEIRExpr>> argExprsIn)
    : FEIRStmt(kFEIRStmtNary), op(opIn), argExprs(std::move(argExprsIn)) {}

std::list<StmtNode*> FEIRStmtNary::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> stmts;
  StmtNode *stmt = nullptr;
  if (argExprs.size() > 1) {
    MapleVector<BaseNode*> args(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());
    for (const auto &arg : argExprs) {
      BaseNode *node = arg->GenMIRNode(mirBuilder);
      args.push_back(node);
    }
    stmt = mirBuilder.CreateStmtNary(op, args);
  } else if (argExprs.size() == 1) {
    BaseNode *node = argExprs.front()->GenMIRNode(mirBuilder);
    stmt = mirBuilder.CreateStmtNary(op, node);
  } else {
    CHECK_FATAL(false, "Invalid arg size for MIR StmtNary");
  }
  stmts.emplace_back(stmt);
  return stmts;
}

// ---------- FEStmtAssign ----------
FEIRStmtAssign::FEIRStmtAssign(FEIRNodeKind argKind, std::unique_ptr<FEIRVar> argVar)
    : FEIRStmt(argKind),
      hasException(false),
      var(std::move(argVar)) {}

void FEIRStmtAssign::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  if (var != nullptr) {
    var->SetDef(true);
    checkPoint.RegisterDFGNode(var);
  }
}

std::string FEIRStmtAssign::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  return ss.str();
}

// ---------- FEStmtDAssign ----------
FEIRStmtDAssign::FEIRStmtDAssign(std::unique_ptr<FEIRVar> argVar, std::unique_ptr<FEIRExpr> argExpr, int32 argFieldID)
    : FEIRStmtAssign(FEIRNodeKind::kStmtDAssign, std::move(argVar)),
      fieldID(argFieldID) {
  SetExpr(std::move(argExpr));
}

void FEIRStmtDAssign::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  expr->RegisterDFGNodes2CheckPoint(checkPoint);
  var->SetDef(true);
  checkPoint.RegisterDFGNode(var);
}

bool FEIRStmtDAssign::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  return expr->CalculateDefs4AllUses(checkPoint, udChain);
}

void FEIRStmtDAssign::InitTrans4AllVarsImpl() {
  switch (expr->GetKind()) {
    case FEIRNodeKind::kExprDRead: {
      FEIRExprDRead *dRead = static_cast<FEIRExprDRead*>(expr.get());
      UniqueFEIRVarTrans trans1 = std::make_unique<FEIRVarTrans>(FEIRVarTransKind::kFEIRVarTransDirect, var);
      dRead->SetTrans(std::move(trans1));
      var->SetTrans(dRead->CreateTransDirect());
      break;
    }
    case FEIRNodeKind::kExprArrayLoad: {
      FEIRExprArrayLoad *arrayLoad = static_cast<FEIRExprArrayLoad*>(expr.get());
      UniqueFEIRVarTrans trans1 = std::make_unique<FEIRVarTrans>(FEIRVarTransKind::kFEIRVarTransArrayDimIncr, var);
      arrayLoad->SetTrans(std::move(trans1));
      var->SetTrans(arrayLoad->CreateTransArrayDimDecr());
      break;
    }
    default:
      break;
  }
}

std::list<StmtNode*> FEIRStmtDAssign::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  ASSERT(var != nullptr, "dst var is nullptr");
  ASSERT(expr != nullptr, "src expr is nullptr");
  MIRSymbol *dstSym = var->GenerateMIRSymbol(mirBuilder);
  BaseNode *srcNode = expr->GenMIRNode(mirBuilder);
  StmtNode *mirStmt = mirBuilder.CreateStmtDassign(*dstSym, fieldID, srcNode);
  ans.push_back(mirStmt);
  return ans;
}

std::string FEIRStmtDAssign::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  ss << " def : " << var->GetNameRaw() << ", uses : " << expr->DumpDotString() << std::endl;
  return ss.str();
}

// ---------- FEIRStmtJavaTypeCheck ----------
FEIRStmtJavaTypeCheck::FEIRStmtJavaTypeCheck(std::unique_ptr<FEIRVar> argVar, std::unique_ptr<FEIRExpr> argExpr,
                                             std::unique_ptr<FEIRType> argType,
                                             FEIRStmtJavaTypeCheck::CheckKind argCheckKind)
    : FEIRStmtAssign(FEIRNodeKind::kStmtJavaTypeCheck, std::move(argVar)),
      checkKind(argCheckKind),
      expr(std::move(argExpr)),
      type(std::move(argType)) {}

FEIRStmtJavaTypeCheck::FEIRStmtJavaTypeCheck(std::unique_ptr<FEIRVar> argVar, std::unique_ptr<FEIRExpr> argExpr,
                                             std::unique_ptr<FEIRType> argType,
                                             FEIRStmtJavaTypeCheck::CheckKind argCheckKind,
                                             uint32 argTypeID)
    : FEIRStmtAssign(FEIRNodeKind::kStmtJavaTypeCheck, std::move(argVar)),
      checkKind(argCheckKind),
      typeID(argTypeID),
      expr(std::move(argExpr)),
      type(std::move(argType)) {}

void FEIRStmtJavaTypeCheck::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  expr->RegisterDFGNodes2CheckPoint(checkPoint);
  var->SetDef(true);
  checkPoint.RegisterDFGNode(var);
}

bool FEIRStmtJavaTypeCheck::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  return expr->CalculateDefs4AllUses(checkPoint, udChain);
}

std::list<StmtNode*> FEIRStmtJavaTypeCheck::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  CHECK_FATAL(expr->GetKind() == FEIRNodeKind::kExprDRead, "only support expr dread");
  BaseNode *objNode = expr->GenMIRNode(mirBuilder);
  MIRSymbol *ret = var->GenerateLocalMIRSymbol(mirBuilder);
  MIRType *mirType = type->GenerateMIRType();
  MIRType *mirPtrType = GlobalTables::GetTypeTable().GetOrCreatePointerType(*mirType, PTY_ref);
  MapleVector<BaseNode*> arguments(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());
  arguments.push_back(objNode);
  if (checkKind == kCheckCast) {
    StmtNode *callStmt = mirBuilder.CreateStmtIntrinsicCallAssigned(INTRN_JAVA_CHECK_CAST, arguments, ret,
                                                                    mirPtrType->GetTypeIndex());
    ans.push_back(callStmt);
  } else {
    BaseNode *instanceOf = mirBuilder.CreateExprIntrinsicop(INTRN_JAVA_INSTANCE_OF, OP_intrinsicopwithtype, *mirPtrType,
                                                            arguments);
    instanceOf->SetPrimType(PTY_u1);
    DassignNode *stmt = mirBuilder.CreateStmtDassign(*ret, 0, instanceOf);
    ans.push_back(stmt);
  }
  return ans;
}

std::string FEIRStmtJavaTypeCheck::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  ss << "uses :" << expr->DumpDotString();
  return ss.str();
}

// ---------- FEIRStmtJavaConstClass ----------
FEIRStmtJavaConstClass::FEIRStmtJavaConstClass(std::unique_ptr<FEIRVar> argVar, std::unique_ptr<FEIRType> argType)
    : FEIRStmtAssign(FEIRNodeKind::kStmtJavaConstClass, std::move(argVar)),
      type(std::move(argType)) {}

void FEIRStmtJavaConstClass::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  var->SetDef(true);
  checkPoint.RegisterDFGNode(var);
}

std::list<StmtNode*> FEIRStmtJavaConstClass::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  MIRSymbol *varSym = var->GenerateLocalMIRSymbol(mirBuilder);
  MapleVector<BaseNode*> args(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());
  MIRType *ptrType = type->GenerateMIRTypeAuto(kSrcLangJava);
  BaseNode *expr =
      mirBuilder.CreateExprIntrinsicop(INTRN_JAVA_CONST_CLASS, OP_intrinsicopwithtype, *ptrType, args);
  StmtNode *stmt = mirBuilder.CreateStmtDassign(*varSym, 0, expr);
  ans.push_back(stmt);
  return ans;
}

// ---------- FEIRStmtJavaConstString ----------
FEIRStmtJavaConstString::FEIRStmtJavaConstString(std::unique_ptr<FEIRVar> argVar, const GStrIdx &argStrIdx)
    : FEIRStmtAssign(FEIRNodeKind::kStmtJavaConstString, std::move(argVar)),
      strIdx(argStrIdx) {}

FEIRStmtJavaConstString::FEIRStmtJavaConstString(std::unique_ptr<FEIRVar> argVar, const GStrIdx &argStrIdx,
                                                 uint32 argStringID)
    : FEIRStmtAssign(FEIRNodeKind::kStmtJavaConstString, std::move(argVar)),
      strIdx(argStrIdx), stringID(argStringID) {}

void FEIRStmtJavaConstString::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  var->SetDef(true);
  checkPoint.RegisterDFGNode(var);
}

std::list<StmtNode*> FEIRStmtJavaConstString::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  const std::string &str = GlobalTables::GetStrTable().GetStringFromStrIdx(strIdx);
  MIRSymbol *literalVal = FEManager::GetJavaStringManager().GetLiteralVar(str);
  if (literalVal == nullptr) {
    literalVal = FEManager::GetJavaStringManager().CreateLiteralVar(mirBuilder, str, false);
  }
  MIRSymbol *literalValPtr = FEManager::GetJavaStringManager().GetLiteralPtrVar(literalVal);
  if (literalValPtr == nullptr) {
    std::string localStrName = kLocalStringPrefix + std::to_string(static_cast<uint32>(strIdx));
    MIRType *typeString = FETypeManager::kFEIRTypeJavaString->GenerateMIRTypeAuto(kSrcLangJava);
    MIRSymbol *symbolLocal = mirBuilder.GetOrCreateLocalDecl(localStrName.c_str(), *typeString);
    if (!FEOptions::GetInstance().IsAOT()) {
      MapleVector<BaseNode*> args(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());
      args.push_back(mirBuilder.CreateExprAddrof(0, *literalVal));
      StmtNode *stmtCreate = mirBuilder.CreateStmtCallAssigned(
          FEManager::GetTypeManager().GetPuIdxForMCCGetOrInsertLiteral(), args, symbolLocal, OP_callassigned);

      ans.push_back(stmtCreate);
    }
    literalValPtr = symbolLocal;
  }
  (void)stringID;
  MIRSymbol *varDst = var->GenerateLocalMIRSymbol(mirBuilder);
  AddrofNode *node = mirBuilder.CreateDread(*literalValPtr, PTY_ptr);
  StmtNode *stmt = mirBuilder.CreateStmtDassign(*varDst, 0, node);
  ans.push_back(stmt);
  return ans;
}

// ---------- FEIRStmtJavaFillArrayData ----------
FEIRStmtJavaFillArrayData::FEIRStmtJavaFillArrayData(std::unique_ptr<FEIRExpr> arrayExprIn, const int8 *arrayDataIn,
                                                     uint32 sizeIn, const std::string &arrayNameIn)
    : FEIRStmtAssign(FEIRNodeKind::kStmtJavaFillArrayData, nullptr),
      arrayExpr(std::move(arrayExprIn)),
      arrayData(arrayDataIn),
      size(sizeIn),
      arrayName(arrayNameIn) {}

void FEIRStmtJavaFillArrayData::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  arrayExpr->RegisterDFGNodes2CheckPoint(checkPoint);
}

bool FEIRStmtJavaFillArrayData::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  return arrayExpr->CalculateDefs4AllUses(checkPoint, udChain);
}

std::list<StmtNode*> FEIRStmtJavaFillArrayData::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  PrimType elemPrimType = ProcessArrayElemPrimType();
  MapleVector<BaseNode*> args(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());
  BaseNode *nodeArray = arrayExpr->GenMIRNode(mirBuilder);
  args.push_back(nodeArray);
  MIRSymbol *elemDataVar = ProcessArrayElemData(mirBuilder, elemPrimType);
  BaseNode *nodeAddrof = mirBuilder.CreateExprAddrof(0, *elemDataVar);
  args.push_back(nodeAddrof);
  uint64 elemPrimTypeSize = GetPrimTypeSize(elemPrimType);
  uint64 val = elemPrimTypeSize * size;
  BaseNode *nodebytes = mirBuilder.CreateIntConst(val, PTY_i32);
  args.push_back(nodebytes);
  StmtNode *stmt = mirBuilder.CreateStmtIntrinsicCallAssigned(INTRN_JAVA_ARRAY_FILL, args, nullptr);
  ans.push_back(stmt);
  return ans;
}

PrimType FEIRStmtJavaFillArrayData::ProcessArrayElemPrimType() const {
  const FEIRType &arrayType = arrayExpr->GetTypeRef();
  CHECK_FATAL(arrayType.IsArray(), "var should be array type");
  UniqueFEIRType elemType = arrayType.Clone();
  (void)elemType->ArrayDecrDim();
  PrimType elemPrimType = elemType->GetPrimType();
  return elemPrimType;
}

MIRSymbol *FEIRStmtJavaFillArrayData::ProcessArrayElemData(MIRBuilder &mirBuilder, PrimType elemPrimType) const {
  // specify size for const array
  uint32 sizeIn = size;
  MIRType *arrayTypeWithSize = GlobalTables::GetTypeTable().GetOrCreateArrayType(
      *GlobalTables::GetTypeTable().GetPrimType(elemPrimType), 1, &sizeIn);
  MIRSymbol *arrayVar = mirBuilder.GetOrCreateGlobalDecl(arrayName, *arrayTypeWithSize);
  arrayVar->SetAttr(ATTR_readonly);
  arrayVar->SetStorageClass(kScFstatic);
  MIRAggConst *val = FillArrayElem(mirBuilder, elemPrimType, *arrayTypeWithSize, arrayData, size);
  arrayVar->SetKonst(val);
  return arrayVar;
}

MIRAggConst *FEIRStmtJavaFillArrayData::FillArrayElem(MIRBuilder &mirBuilder, PrimType elemPrimType,
                                                      MIRType &arrayTypeWithSize, const int8 *arrayData,
                                                      uint32 size) const {
  MemPool *mp = mirBuilder.GetMirModule().GetMemPool();
  MIRModule &module = mirBuilder.GetMirModule();
  MIRAggConst *val = module.GetMemPool()->New<MIRAggConst>(module, arrayTypeWithSize);
  MIRConst *cst = nullptr;
  for (uint32 i = 0; i < size; ++i) {
    MIRType &elemType = *GlobalTables::GetTypeTable().GetPrimType(elemPrimType);
    switch (elemPrimType) {
      case PTY_u1:
        cst = mp->New<MIRIntConst>((reinterpret_cast<const bool*>(arrayData))[i], elemType);
        break;
      case PTY_i8:
        cst = mp->New<MIRIntConst>((reinterpret_cast<const int8*>(arrayData))[i], elemType);
        break;
      case PTY_u8:
        cst = mp->New<MIRIntConst>((reinterpret_cast<const uint8*>(arrayData))[i], elemType);
        break;
      case PTY_i16:
        cst = mp->New<MIRIntConst>((reinterpret_cast<const int16*>(arrayData))[i], elemType);
        break;
      case PTY_u16:
        cst = mp->New<MIRIntConst>((reinterpret_cast<const uint16*>(arrayData))[i], elemType);
        break;
      case PTY_i32:
        cst = mp->New<MIRIntConst>((reinterpret_cast<const int32*>(arrayData))[i], elemType);
        break;
      case PTY_u32:
        cst = mp->New<MIRIntConst>((reinterpret_cast<const uint32*>(arrayData))[i], elemType);
        break;
      case PTY_i64:
        cst = mp->New<MIRIntConst>((reinterpret_cast<const int64*>(arrayData))[i], elemType);
        break;
      case PTY_u64:
        cst = mp->New<MIRIntConst>((reinterpret_cast<const uint64*>(arrayData))[i], elemType);
        break;
      case PTY_f32:
        cst = mp->New<MIRFloatConst>((reinterpret_cast<const float*>(arrayData))[i], elemType);
        break;
      case PTY_f64:
        cst = mp->New<MIRDoubleConst>((reinterpret_cast<const double*>(arrayData))[i], elemType);
        break;
      default:
        CHECK_FATAL(false, "Unsupported primitive type");
        break;
    }
    val->PushBack(cst);
  }
  return val;
}

// ---------- FEIRStmtJavaMultiANewArray ----------
UniqueFEIRVar FEIRStmtJavaMultiANewArray::varSize = nullptr;
UniqueFEIRVar FEIRStmtJavaMultiANewArray::varClass = nullptr;
UniqueFEIRType FEIRStmtJavaMultiANewArray::typeAnnotation = nullptr;
FEStructMethodInfo *FEIRStmtJavaMultiANewArray::methodInfoNewInstance = nullptr;

FEIRStmtJavaMultiANewArray::FEIRStmtJavaMultiANewArray(std::unique_ptr<FEIRVar> argVar,
                                                       std::unique_ptr<FEIRType> argType)
    : FEIRStmtAssign(FEIRNodeKind::kStmtJavaMultiANewArray, std::move(argVar)),
      type(std::move(argType)) {}

void FEIRStmtJavaMultiANewArray::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  for (const UniqueFEIRExpr &expr : exprSizes) {
    expr->RegisterDFGNodes2CheckPoint(checkPoint);
  }
  var->SetDef(true);
  checkPoint.RegisterDFGNode(var);
}

bool FEIRStmtJavaMultiANewArray::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  bool success = true;
  for (const UniqueFEIRExpr &expr : exprSizes) {
    success = success && expr->CalculateDefs4AllUses(checkPoint, udChain);
  }
  return success;
}

void FEIRStmtJavaMultiANewArray::AddVarSize(std::unique_ptr<FEIRVar> argVarSize) {
  argVarSize->SetType(FETypeManager::kPrimFEIRTypeI32->Clone());
  UniqueFEIRExpr expr = FEIRBuilder::CreateExprDRead(std::move(argVarSize));
  exprSizes.push_back(std::move(expr));
}

void FEIRStmtJavaMultiANewArray::AddVarSizeRev(std::unique_ptr<FEIRVar> argVarSize) {
  argVarSize->SetType(FETypeManager::kPrimFEIRTypeI32->Clone());
  UniqueFEIRExpr expr = FEIRBuilder::CreateExprDRead(std::move(argVarSize));
  exprSizes.push_front(std::move(expr));
}

std::list<StmtNode*> FEIRStmtJavaMultiANewArray::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  // size array fill
  MapleVector<BaseNode*> argsSizeArrayFill(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());
  for (const UniqueFEIRExpr &expr : exprSizes) {
    BaseNode *node = expr->GenMIRNode(mirBuilder);
    argsSizeArrayFill.push_back(node);
  }
  MIRSymbol *symSize = GetVarSize()->GenerateLocalMIRSymbol(mirBuilder);
  StmtNode *stmtSizeArrayFill = mirBuilder.CreateStmtIntrinsicCallAssigned(INTRN_JAVA_ARRAY_FILL, argsSizeArrayFill,
                                                                           symSize, TyIdx(PTY_i32));
  ans.push_back(stmtSizeArrayFill);
  // class annotation
  FEIRStmtJavaConstClass feStmtConstClass(GetVarClass()->Clone(), GetTypeAnnotation()->Clone());
  std::list<StmtNode*> stmtsConstClass = feStmtConstClass.GenMIRStmts(mirBuilder);
  (void)ans.insert(ans.end(), stmtsConstClass.begin(), stmtsConstClass.end());
  // invoke newInstance
  UniqueFEIRVar varRetCall = var->Clone();
  varRetCall->SetType(FETypeManager::kFEIRTypeJavaObject->Clone());
  FEIRStmtCallAssign feStmtCall(GetMethodInfoNewInstance(), OP_call, varRetCall->Clone(), true);
  feStmtCall.AddExprArg(FEIRBuilder::CreateExprDRead(GetVarClass()->Clone()));
  feStmtCall.AddExprArg(FEIRBuilder::CreateExprDRead(GetVarSize()->Clone()));
  std::list<StmtNode*> stmtsCall = feStmtCall.GenMIRStmts(mirBuilder);
  (void)ans.insert(ans.end(), stmtsCall.begin(), stmtsCall.end());
  // check cast
  var->SetType(type->Clone());
  UniqueFEIRExpr expr = std::make_unique<FEIRExprDRead>(std::move(varRetCall));
  FEIRStmtJavaTypeCheck feStmtCheck(var->Clone(), std::move(expr), type->Clone(), FEIRStmtJavaTypeCheck::kCheckCast);
  std::list<StmtNode*> stmtsCheck = feStmtCheck.GenMIRStmts(mirBuilder);
  (void)ans.insert(ans.end(), stmtsCheck.begin(), stmtsCheck.end());
  return ans;
}

const UniqueFEIRVar &FEIRStmtJavaMultiANewArray::GetVarSize() {
  if (varSize != nullptr) {
    return varSize;
  }
  MPLFE_PARALLEL_FORBIDDEN();
  GStrIdx varNameIdx = GlobalTables::GetStrTable().GetOrCreateStrIdxFromName("tmpsize");
  UniqueFEIRType varSizeType = FETypeManager::kPrimFEIRTypeI32->Clone();
  (void)varSizeType->ArrayIncrDim();
  varSize = std::make_unique<FEIRVarName>(varNameIdx, std::move(varSizeType), true);
  return varSize;
}

const UniqueFEIRVar &FEIRStmtJavaMultiANewArray::GetVarClass() {
  if (varClass != nullptr) {
    return varClass;
  }
  MPLFE_PARALLEL_FORBIDDEN();
  GStrIdx varNameIdx = GlobalTables::GetStrTable().GetOrCreateStrIdxFromName("tmpclass");
  varClass = std::make_unique<FEIRVarName>(varNameIdx, FETypeManager::kFEIRTypeJavaClass->Clone(), true);
  return varClass;
}

const UniqueFEIRType &FEIRStmtJavaMultiANewArray::GetTypeAnnotation() {
  if (typeAnnotation != nullptr) {
    return typeAnnotation;
  }
  MPLFE_PARALLEL_FORBIDDEN();
  typeAnnotation = std::make_unique<FEIRTypeDefault>(PTY_ref);
  static_cast<FEIRTypeDefault*>(typeAnnotation.get())->LoadFromJavaTypeName("Ljava/lang/annotation/Annotation;", false);
  return typeAnnotation;
}

FEStructMethodInfo &FEIRStmtJavaMultiANewArray::GetMethodInfoNewInstance() {
  if (methodInfoNewInstance != nullptr) {
    return *methodInfoNewInstance;
  }
  StructElemNameIdx structElemNameIdx(bc::BCUtil::GetMultiANewArrayClassIdx(),
                                      bc::BCUtil::GetMultiANewArrayElemIdx(),
                                      bc::BCUtil::GetMultiANewArrayTypeIdx(),
                                      bc::BCUtil::GetMultiANewArrayFullIdx());
  methodInfoNewInstance = static_cast<FEStructMethodInfo*>(FEManager::GetTypeManager().RegisterStructMethodInfo(
      structElemNameIdx, kSrcLangJava, true));
  return *methodInfoNewInstance;
}

std::string FEIRStmtJavaMultiANewArray::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  return ss.str();
}

// ---------- FEIRStmtUseOnly ----------
FEIRStmtUseOnly::FEIRStmtUseOnly(FEIRNodeKind argKind, Opcode argOp, std::unique_ptr<FEIRExpr> argExpr)
    : FEIRStmt(argKind),
      op(argOp) {
  if (argExpr != nullptr) {
    expr = std::move(argExpr);
  }
}

FEIRStmtUseOnly::FEIRStmtUseOnly(Opcode argOp, std::unique_ptr<FEIRExpr> argExpr)
    : FEIRStmtUseOnly(FEIRNodeKind::kStmtUseOnly, argOp, std::move(argExpr)) {}

void FEIRStmtUseOnly::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  if (expr != nullptr) {
    expr->RegisterDFGNodes2CheckPoint(checkPoint);
  }
}

bool FEIRStmtUseOnly::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  if (expr != nullptr) {
    return expr->CalculateDefs4AllUses(checkPoint, udChain);
  }
  return true;
}

std::list<StmtNode*> FEIRStmtUseOnly::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  ASSERT_NOT_NULL(expr);
  BaseNode *srcNode = expr->GenMIRNode(mirBuilder);
  StmtNode *mirStmt = mirBuilder.CreateStmtNary(op, srcNode);
  ans.push_back(mirStmt);
  return ans;
}

std::string FEIRStmtUseOnly::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  if (expr != nullptr) {
    ss << expr->DumpDotString();
  }
  return ss.str();
}

// ---------- FEIRStmtReturn ----------
FEIRStmtReturn::FEIRStmtReturn(std::unique_ptr<FEIRExpr> argExpr)
    : FEIRStmtUseOnly(FEIRNodeKind::kStmtReturn, OP_return, std::move(argExpr)) {}

std::list<StmtNode*> FEIRStmtReturn::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  StmtNode *mirStmt = nullptr;
  if (expr == nullptr) {
    mirStmt = mirBuilder.CreateStmtReturn(nullptr);
  } else {
    BaseNode *srcNode = expr->GenMIRNode(mirBuilder);
    mirStmt = mirBuilder.CreateStmtReturn(srcNode);
  }
  ans.push_back(mirStmt);
  return ans;
}

// ---------- FEIRStmtGoto ----------
FEIRStmtGoto::FEIRStmtGoto(uint32 argLabelIdx)
    : FEIRStmt(FEIRNodeKind::kStmtGoto),
      labelIdx(argLabelIdx),
      stmtTarget(nullptr) {}

FEIRStmtGoto::~FEIRStmtGoto() {
  stmtTarget = nullptr;
}

std::list<StmtNode*> FEIRStmtGoto::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  CHECK_NULL_FATAL(stmtTarget);
  GotoNode *gotoNode = mirBuilder.CreateStmtGoto(OP_goto, stmtTarget->GetMIRLabelIdx());
  ans.push_back(gotoNode);
  return ans;
}

std::string FEIRStmtGoto::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  return ss.str();
}

// ---------- FEIRStmtGoto2 ----------
FEIRStmtGoto2::FEIRStmtGoto2(uint32 qIdx0, uint32 qIdx1)
    : FEIRStmt(FEIRNodeKind::kStmtGoto), labelIdxOuter(qIdx0), labelIdxInner(qIdx1) {}

std::list<StmtNode*> FEIRStmtGoto2::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> stmts;
  GotoNode *gotoNode = mirBuilder.CreateStmtGoto(
      OP_goto, FEIRStmtPesudoLabel2::GenMirLabelIdx(mirBuilder, labelIdxOuter, labelIdxInner));
  stmts.push_back(gotoNode);
  return stmts;
}

std::pair<uint32, uint32> FEIRStmtGoto2::GetLabelIdx() const {
  return std::make_pair(labelIdxOuter, labelIdxInner);
}

// ---------- FEIRStmtCondGoto ----------
FEIRStmtCondGoto::FEIRStmtCondGoto(Opcode argOp, uint32 argLabelIdx, UniqueFEIRExpr argExpr)
    : FEIRStmtGoto(argLabelIdx),
      op(argOp) {
  kind = FEIRNodeKind::kStmtCondGoto;
  SetExpr(std::move(argExpr));
}

void FEIRStmtCondGoto::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  expr->RegisterDFGNodes2CheckPoint(checkPoint);
}

bool FEIRStmtCondGoto::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  return expr->CalculateDefs4AllUses(checkPoint, udChain);
}

std::list<StmtNode*> FEIRStmtCondGoto::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  BaseNode *condNode = expr->GenMIRNode(mirBuilder);
  CHECK_NULL_FATAL(stmtTarget);
  CondGotoNode *gotoNode = mirBuilder.CreateStmtCondGoto(condNode, op, stmtTarget->GetMIRLabelIdx());
  ans.push_back(gotoNode);
  return ans;
}

std::string FEIRStmtCondGoto::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  return ss.str();
}

// ---------- FEIRStmtCondGoto2 ----------
FEIRStmtCondGoto2::FEIRStmtCondGoto2(Opcode argOp, uint32 qIdx0, uint32 qIdx1, UniqueFEIRExpr argExpr)
    : FEIRStmtGoto2(qIdx0, qIdx1), op(argOp), expr(std::move(argExpr)) {}

std::list<StmtNode*> FEIRStmtCondGoto2::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> stmts;
  BaseNode *condNode = expr->GenMIRNode(mirBuilder);
  CondGotoNode *gotoNode = mirBuilder.CreateStmtCondGoto(
      condNode, op, FEIRStmtPesudoLabel2::GenMirLabelIdx(mirBuilder, labelIdxOuter, labelIdxInner));
  stmts.push_back(gotoNode);
  return stmts;
}

// ---------- FEIRStmtSwitch ----------
FEIRStmtSwitch::FEIRStmtSwitch(UniqueFEIRExpr argExpr)
    : FEIRStmt(FEIRNodeKind::kStmtSwitch),
      defaultLabelIdx(0),
      defaultTarget(nullptr) {
  SetExpr(std::move(argExpr));
}

FEIRStmtSwitch::~FEIRStmtSwitch() {
  defaultTarget = nullptr;
}

void FEIRStmtSwitch::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  expr->RegisterDFGNodes2CheckPoint(checkPoint);
}

bool FEIRStmtSwitch::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  return expr->CalculateDefs4AllUses(checkPoint, udChain);
}

std::list<StmtNode*> FEIRStmtSwitch::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  CaseVector switchTable(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());
  for (const std::pair<const int32, FEIRStmtPesudoLabel*> &targetPair : mapValueTargets) {
    CHECK_NULL_FATAL(targetPair.second);
    switchTable.push_back(std::make_pair(targetPair.first, targetPair.second->GetMIRLabelIdx()));
  }
  BaseNode *exprNode = expr->GenMIRNode(mirBuilder);
  CHECK_NULL_FATAL(defaultTarget);
  SwitchNode *switchNode = mirBuilder.CreateStmtSwitch(exprNode, defaultTarget->GetMIRLabelIdx(), switchTable);
  ans.push_back(switchNode);
  return ans;
}

std::string FEIRStmtSwitch::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  return ss.str();
}

// ---------- FEIRStmtSwitch2 ----------
FEIRStmtSwitch2::FEIRStmtSwitch2(uint32 outerIdxIn, UniqueFEIRExpr argExpr)
    : FEIRStmt(FEIRNodeKind::kStmtSwitch),
      outerIdx(outerIdxIn),
      defaultLabelIdx(0),
      defaultTarget(nullptr) {
  SetExpr(std::move(argExpr));
}

FEIRStmtSwitch2::~FEIRStmtSwitch2() {
  defaultTarget = nullptr;
}

std::list<StmtNode*> FEIRStmtSwitch2::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  CaseVector switchTable(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());
  for (const std::pair<const int32, uint32> &valueLabelPair : mapValueLabelIdx) {
    switchTable.emplace_back(valueLabelPair.first,
                             FEIRStmtPesudoLabel2::GenMirLabelIdx(mirBuilder, outerIdx, valueLabelPair.second));
  }
  BaseNode *exprNode = expr->GenMIRNode(mirBuilder);
  SwitchNode *switchNode = mirBuilder.CreateStmtSwitch(exprNode,
      FEIRStmtPesudoLabel2::GenMirLabelIdx(mirBuilder, outerIdx, defaultLabelIdx), switchTable);
  ans.push_back(switchNode);
  return ans;
}

std::string FEIRStmtSwitch2::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  return ss.str();
}

// ---------- FEIRStmtArrayStore ----------
FEIRStmtArrayStore::FEIRStmtArrayStore(UniqueFEIRExpr argExprElem, UniqueFEIRExpr argExprArray,
                                       UniqueFEIRExpr argExprIndex, UniqueFEIRType argTypeArray)
    : FEIRStmt(FEIRNodeKind::kStmtArrayStore),
      exprElem(std::move(argExprElem)),
      exprArray(std::move(argExprArray)),
      exprIndex(std::move(argExprIndex)),
      typeArray(std::move(argTypeArray)) {}

void FEIRStmtArrayStore::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  exprArray->RegisterDFGNodes2CheckPoint(checkPoint);
  exprIndex->RegisterDFGNodes2CheckPoint(checkPoint);
  exprElem->RegisterDFGNodes2CheckPoint(checkPoint);
}

bool FEIRStmtArrayStore::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  bool success = true;
  success = success && exprArray->CalculateDefs4AllUses(checkPoint, udChain);
  success = success && exprIndex->CalculateDefs4AllUses(checkPoint, udChain);
  success = success && exprElem->CalculateDefs4AllUses(checkPoint, udChain);
  return success;
}

void FEIRStmtArrayStore::InitTrans4AllVarsImpl() {
  CHECK_FATAL(exprArray->GetKind() == kExprDRead, "only support dread expr for exprArray");
  CHECK_FATAL(exprIndex->GetKind() == kExprDRead, "only support dread expr for exprIndex");
  CHECK_FATAL(exprElem->GetKind() == kExprDRead, "only support dread expr for exprElem");
  FEIRExprDRead *exprArrayDRead = static_cast<FEIRExprDRead*>(exprArray.get());
  FEIRExprDRead *exprElemDRead = static_cast<FEIRExprDRead*>(exprElem.get());
  exprArrayDRead->SetTrans(exprElemDRead->CreateTransArrayDimIncr());
  exprElemDRead->SetTrans(exprArrayDRead->CreateTransArrayDimDecr());
}

std::list<StmtNode*> FEIRStmtArrayStore::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  CHECK_FATAL(exprArray->GetKind() == kExprDRead, "only support dread expr for exprArray");
  CHECK_FATAL((exprIndex->GetKind() == kExprDRead) || (exprIndex->GetKind() == kExprConst), "only support dread/const"\
                                                                                            "expr for exprIndex");
  CHECK_FATAL(exprElem->GetKind() == kExprDRead, "only support dread expr for exprElem");
  BaseNode *addrBase = exprArray->GenMIRNode(mirBuilder);
  BaseNode *indexBn = exprIndex->GenMIRNode(mirBuilder);
  MIRType *ptrMIRArrayType = typeArray->GenerateMIRType(false);
  BaseNode *arrayExpr = mirBuilder.CreateExprArray(*ptrMIRArrayType, addrBase, indexBn);
  UniqueFEIRType typeElem = typeArray->Clone();
  if ((exprIndex->GetKind() != kExprConst) || (!FEOptions::GetInstance().IsAOT())) {
    (void)typeElem->ArrayDecrDim();
  }
  MIRType *mIRElemType = typeElem->GenerateMIRType(true);
  BaseNode *elemBn = exprElem->GenMIRNode(mirBuilder);
  IassignNode *stmt = nullptr;
  if ((exprIndex->GetKind() != kExprConst) || (!FEOptions::GetInstance().IsAOT())) {
    MIRType *ptrMIRElemType = GlobalTables::GetTypeTable().GetOrCreatePointerType(*mIRElemType, PTY_ptr);
    stmt = mirBuilder.CreateStmtIassign(*ptrMIRElemType, 0, arrayExpr, elemBn);
  } else {
    reinterpret_cast<ArrayNode *>(arrayExpr)->SetBoundsCheck(false);
    stmt = mirBuilder.CreateStmtIassign(*mIRElemType, 0, arrayExpr, elemBn);
  }
  return std::list<StmtNode*>({ stmt });
}

std::string FEIRStmtArrayStore::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  ss << " def : {";
  ss << " exprArray : " << exprArray->DumpDotString();
  ss << " exprIndex : " << exprIndex->DumpDotString();
  ss << " exprElem kind : " << GetFEIRNodeKindDescription(exprElem->GetKind()) << " " << exprElem->DumpDotString();
  ss << " } ";
  return ss.str();
}

// ---------- FEIRStmtFieldStore ----------
FEIRStmtFieldStore::FEIRStmtFieldStore(UniqueFEIRVar argVarObj, UniqueFEIRVar argVarField,
                                       FEStructFieldInfo &argFieldInfo, bool argIsStatic)
    : FEIRStmt(FEIRNodeKind::kStmtFieldStore),
      varObj(std::move(argVarObj)),
      varField(std::move(argVarField)),
      fieldInfo(argFieldInfo),
      isStatic(argIsStatic) {}

void FEIRStmtFieldStore::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  if (isStatic) {
    RegisterDFGNodes2CheckPointForStatic(checkPoint);
  } else {
    RegisterDFGNodes2CheckPointForNonStatic(checkPoint);
  }
}

void FEIRStmtFieldStore::RegisterDFGNodes2CheckPointForStatic(FEIRStmtCheckPoint &checkPoint) {
  checkPoint.RegisterDFGNode(varField);
}

void FEIRStmtFieldStore::RegisterDFGNodes2CheckPointForNonStatic(FEIRStmtCheckPoint &checkPoint) {
  checkPoint.RegisterDFGNode(varObj);
  checkPoint.RegisterDFGNode(varField);
}

bool FEIRStmtFieldStore::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  if (isStatic) {
    return CalculateDefs4AllUsesForStatic(checkPoint, udChain);
  } else {
    return CalculateDefs4AllUsesForNonStatic(checkPoint, udChain);
  }
}

bool FEIRStmtFieldStore::CalculateDefs4AllUsesForStatic(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  std::set<UniqueFEIRVar*> &defs4VarField = checkPoint.CalcuDef(varField);
  (void)udChain.insert(std::make_pair(&varField, defs4VarField));
  return (defs4VarField.size() > 0);
}

bool FEIRStmtFieldStore::CalculateDefs4AllUsesForNonStatic(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  std::set<UniqueFEIRVar*> &defs4VarObj = checkPoint.CalcuDef(varObj);
  (void)udChain.insert(std::make_pair(&varObj, defs4VarObj));
  std::set<UniqueFEIRVar*> &defs4VarField = checkPoint.CalcuDef(varField);
  (void)udChain.insert(std::make_pair(&varField, defs4VarField));
  return ((defs4VarObj.size() > 0) && (defs4VarField.size() > 0));
}

std::list<StmtNode*> FEIRStmtFieldStore::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  // prepare and find root
  fieldInfo.Prepare(mirBuilder, isStatic);
  if (isStatic) {
    return GenMIRStmtsImplForStatic(mirBuilder);
  } else {
    return GenMIRStmtsImplForNonStatic(mirBuilder);
  }
}

uint32 FEIRStmtFieldStore::GetTypeIDForStatic() const {
  const std::string &actualContainerName = fieldInfo.GetActualContainerName();
  uint32 typeID = FEManager::GetTypeManager().GetTypeIDFromMplClassName(actualContainerName);
  return typeID;
}

std::list<StmtNode*> FEIRStmtFieldStore::GenMIRStmtsImplForStatic(MIRBuilder &mirBuilder) const {
  CHECK_FATAL(fieldInfo.GetFieldNameIdx() != 0, "invalid name idx");
  std::list<StmtNode*> mirStmts;
  UniqueFEIRVar varTarget = std::make_unique<FEIRVarName>(fieldInfo.GetFieldNameIdx(), fieldInfo.GetType()->Clone());
  varTarget->SetGlobal(true);
  MIRSymbol *symSrc = varTarget->GenerateGlobalMIRSymbol(mirBuilder);
  UniqueFEIRExpr exprDRead = FEIRBuilder::CreateExprDRead(varField->Clone());
  UniqueFEIRStmt stmtDAssign = FEIRBuilder::CreateStmtDAssign(std::move(varTarget), std::move(exprDRead));
  mirStmts = stmtDAssign->GenMIRStmts(mirBuilder);
  TyIdx containerTyIdx = fieldInfo.GetActualContainerType()->GenerateMIRType()->GetTypeIndex();
  if (!mirBuilder.GetCurrentFunction()->IsClinit() ||
      mirBuilder.GetCurrentFunction()->GetClassTyIdx() != containerTyIdx) {
    MapleVector<BaseNode*> args(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());

    uint32 typeID = UINT32_MAX;
    if (FEOptions::GetInstance().IsAOT()) {
      typeID = GetTypeIDForStatic();
      if (typeID != UINT32_MAX) {
        BaseNode *argNumExpr = mirBuilder.CreateIntConst(static_cast<int32>(typeID), PTY_i32);
        args.push_back(argNumExpr);
      } else {
        const auto &pt = fieldInfo.GetType()->GetPrimType();
        std::map<PrimType, GStrIdx> primTypeFuncNameIdxMap = {
            { PTY_u1, FEUtils::GetMCCStaticFieldSetBoolIdx() },
            { PTY_i8, FEUtils::GetMCCStaticFieldSetByteIdx() },
            { PTY_i16, FEUtils::GetMCCStaticFieldSetShortIdx() },
            { PTY_u16, FEUtils::GetMCCStaticFieldSetCharIdx() },
            { PTY_i32, FEUtils::GetMCCStaticFieldSetIntIdx() },
            { PTY_i64, FEUtils::GetMCCStaticFieldSetLongIdx() },
            { PTY_f32, FEUtils::GetMCCStaticFieldSetFloatIdx() },
            { PTY_f64, FEUtils::GetMCCStaticFieldSetDoubleIdx() },
            { PTY_ref, FEUtils::GetMCCStaticFieldSetObjectIdx() },
        };
        const auto &itorFunc = primTypeFuncNameIdxMap.find(pt);
        CHECK_FATAL(itorFunc != primTypeFuncNameIdxMap.end(), "java type not support %d", pt);
        args.push_back(mirBuilder.CreateIntConst(static_cast<int32>(fieldInfo.GetFieldID()), PTY_i32));
        BaseNode *nodeSrc = mirBuilder.CreateExprDread(*symSrc);
        args.push_back(nodeSrc);
        StmtNode *stmtMCC = mirBuilder.CreateStmtCallAssigned(
            FEManager::GetTypeManager().GetMCCFunction(itorFunc->second)->GetPuidx(), args, nullptr, OP_callassigned);
        mirStmts.emplace_front(stmtMCC);
      }
    }

    if (!FEOptions::GetInstance().IsAOT() || typeID != UINT32_MAX) {
      StmtNode *stmtClinitCheck = mirBuilder.CreateStmtIntrinsicCall(INTRN_JAVA_CLINIT_CHECK, args, containerTyIdx);
      mirStmts.emplace_front(stmtClinitCheck);
    }
  }
  return mirStmts;
}

std::list<StmtNode*> FEIRStmtFieldStore::GenMIRStmtsImplForNonStatic(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  FieldID fieldID = fieldInfo.GetFieldID();
  CHECK_FATAL(fieldID != 0, "invalid field ID");
  MIRStructType *structType = FEManager::GetTypeManager().GetStructTypeFromName(fieldInfo.GetStructName());
  CHECK_NULL_FATAL(structType);
  MIRType *ptrStructType = GlobalTables::GetTypeTable().GetOrCreatePointerType(*structType, PTY_ref);
  UniqueFEIRExpr exprDReadObj = FEIRBuilder::CreateExprDRead(varObj->Clone());
  UniqueFEIRExpr exprDReadField = FEIRBuilder::CreateExprDRead(varField->Clone());
  BaseNode *nodeObj = exprDReadObj->GenMIRNode(mirBuilder);
  BaseNode *nodeField = exprDReadField->GenMIRNode(mirBuilder);
  StmtNode *stmt = mirBuilder.CreateStmtIassign(*ptrStructType, fieldID, nodeObj, nodeField);
  ans.push_back(stmt);
  return ans;
}

std::string FEIRStmtFieldStore::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  return ss.str();
}

// ---------- FEIRStmtFieldLoad ----------
FEIRStmtFieldLoad::FEIRStmtFieldLoad(UniqueFEIRVar argVarObj, UniqueFEIRVar argVarField,
                                     FEStructFieldInfo &argFieldInfo, bool argIsStatic)
    : FEIRStmtAssign(FEIRNodeKind::kStmtFieldLoad, std::move(argVarField)),
      varObj(std::move(argVarObj)),
      fieldInfo(argFieldInfo),
      isStatic(argIsStatic) {}

void FEIRStmtFieldLoad::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  if (isStatic) {
    RegisterDFGNodes2CheckPointForStatic(checkPoint);
  } else {
    RegisterDFGNodes2CheckPointForNonStatic(checkPoint);
  }
}

bool FEIRStmtFieldLoad::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  if (!isStatic) {
    std::set<UniqueFEIRVar*> &defs = checkPoint.CalcuDef(varObj);
    (void)udChain.insert(std::make_pair(&varObj, defs));
    if (defs.size() == 0) {
      return false;
    }
  }
  return true;
}

void FEIRStmtFieldLoad::RegisterDFGNodes2CheckPointForStatic(FEIRStmtCheckPoint &checkPoint) {
  var->SetDef(true);
  checkPoint.RegisterDFGNode(var);
}

void FEIRStmtFieldLoad::RegisterDFGNodes2CheckPointForNonStatic(FEIRStmtCheckPoint &checkPoint) {
  checkPoint.RegisterDFGNode(varObj);
  var->SetDef(true);
  checkPoint.RegisterDFGNode(var);
}

std::list<StmtNode*> FEIRStmtFieldLoad::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  // prepare and find root
  fieldInfo.Prepare(mirBuilder, isStatic);
  if (isStatic) {
    return GenMIRStmtsImplForStatic(mirBuilder);
  } else {
    return GenMIRStmtsImplForNonStatic(mirBuilder);
  }
}

uint32 FEIRStmtFieldLoad::GetTypeIDForStatic() const {
  const std::string &actualContainerName = fieldInfo.GetActualContainerName();
  uint32 typeID = FEManager::GetTypeManager().GetTypeIDFromMplClassName(actualContainerName);
  return typeID;
}

std::list<StmtNode*> FEIRStmtFieldLoad::GenMIRStmtsImplForStatic(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> mirStmts;
  UniqueFEIRVar varTarget = std::make_unique<FEIRVarName>(fieldInfo.GetFieldNameIdx(), fieldInfo.GetType()->Clone());
  varTarget->SetGlobal(true);
  UniqueFEIRExpr exprDRead = FEIRBuilder::CreateExprDRead(std::move(varTarget));
  UniqueFEIRStmt stmtDAssign = FEIRBuilder::CreateStmtDAssign(var->Clone(), std::move(exprDRead));
  mirStmts = stmtDAssign->GenMIRStmts(mirBuilder);
  MapleVector<BaseNode*> args(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());

  uint32 typeID = UINT32_MAX;
  if (FEOptions::GetInstance().IsAOT()) {
    typeID = GetTypeIDForStatic();
    if (typeID != UINT32_MAX) {
      BaseNode *argNumExpr = mirBuilder.CreateIntConst(static_cast<int32>(typeID), PTY_i32);
      args.push_back(argNumExpr);
    } else {
      auto pt = fieldInfo.GetType()->GetPrimType();
      std::map<PrimType, GStrIdx> primTypeFuncNameIdxMap = {
          { PTY_u1, FEUtils::GetMCCStaticFieldGetBoolIdx() },
          { PTY_i8, FEUtils::GetMCCStaticFieldGetByteIdx() },
          { PTY_i16, FEUtils::GetMCCStaticFieldGetShortIdx() },
          { PTY_u16, FEUtils::GetMCCStaticFieldGetCharIdx() },
          { PTY_i32, FEUtils::GetMCCStaticFieldGetIntIdx() },
          { PTY_i64, FEUtils::GetMCCStaticFieldGetLongIdx() },
          { PTY_f32, FEUtils::GetMCCStaticFieldGetFloatIdx() },
          { PTY_f64, FEUtils::GetMCCStaticFieldGetDoubleIdx() },
          { PTY_ref, FEUtils::GetMCCStaticFieldGetObjectIdx() },
      };
      auto itorFunc = primTypeFuncNameIdxMap.find(pt);
      CHECK_FATAL(itorFunc != primTypeFuncNameIdxMap.end(), "java type not support %d", pt);
      args.push_back(mirBuilder.CreateIntConst(static_cast<int32>(fieldInfo.GetFieldID()), PTY_i32));
      StmtNode *stmtMCC = mirBuilder.CreateStmtCallAssigned(
          FEManager::GetTypeManager().GetMCCFunction(itorFunc->second)->GetPuidx(), args, nullptr, OP_callassigned);
      mirStmts.emplace_front(stmtMCC);
    }
  }
  if (!FEOptions::GetInstance().IsAOT() || typeID != UINT32_MAX) {
    TyIdx containerTyIdx = fieldInfo.GetActualContainerType()->GenerateMIRType()->GetTypeIndex();
    if (!mirBuilder.GetCurrentFunction()->IsClinit() ||
        mirBuilder.GetCurrentFunction()->GetClassTyIdx() != containerTyIdx) {
      StmtNode *stmtClinitCheck = mirBuilder.CreateStmtIntrinsicCall(INTRN_JAVA_CLINIT_CHECK, args, containerTyIdx);
      mirStmts.emplace_front(stmtClinitCheck);
    }
  }
  return mirStmts;
}

std::list<StmtNode*> FEIRStmtFieldLoad::GenMIRStmtsImplForNonStatic(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  FieldID fieldID = fieldInfo.GetFieldID();
  CHECK_FATAL(fieldID != 0, "invalid field ID");
  MIRStructType *structType = FEManager::GetTypeManager().GetStructTypeFromName(fieldInfo.GetStructName());
  CHECK_NULL_FATAL(structType);
  MIRType *ptrStructType = GlobalTables::GetTypeTable().GetOrCreatePointerType(*structType, PTY_ref);
  MIRType *fieldType = fieldInfo.GetType()->GenerateMIRTypeAuto(fieldInfo.GetSrcLang());
  UniqueFEIRExpr exprDReadObj = FEIRBuilder::CreateExprDRead(varObj->Clone());
  BaseNode *nodeObj = exprDReadObj->GenMIRNode(mirBuilder);
  BaseNode *nodeVal = mirBuilder.CreateExprIread(*fieldType, *ptrStructType, fieldID, nodeObj);
  MIRSymbol *valRet = var->GenerateLocalMIRSymbol(mirBuilder);
  StmtNode *stmt = mirBuilder.CreateStmtDassign(*valRet, 0, nodeVal);
  ans.push_back(stmt);
  return ans;
}

std::string FEIRStmtFieldLoad::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  return ss.str();
}

// ---------- FEIRStmtCallAssign ----------
std::map<Opcode, Opcode> FEIRStmtCallAssign::mapOpAssignToOp = FEIRStmtCallAssign::InitMapOpAssignToOp();
std::map<Opcode, Opcode> FEIRStmtCallAssign::mapOpToOpAssign = FEIRStmtCallAssign::InitMapOpToOpAssign();

FEIRStmtCallAssign::FEIRStmtCallAssign(FEStructMethodInfo &argMethodInfo, Opcode argMIROp, UniqueFEIRVar argVarRet,
                                       bool argIsStatic)
    : FEIRStmtAssign(FEIRNodeKind::kStmtCallAssign, std::move(argVarRet)),
      methodInfo(argMethodInfo),
      mirOp(argMIROp),
      isStatic(argIsStatic) {}

std::map<Opcode, Opcode> FEIRStmtCallAssign::InitMapOpAssignToOp() {
  std::map<Opcode, Opcode> ans;
  ans[OP_callassigned] = OP_call;
  ans[OP_virtualcallassigned] = OP_virtualcall;
  ans[OP_superclasscallassigned] = OP_superclasscall;
  ans[OP_interfacecallassigned] = OP_interfacecall;
  return ans;
}

std::map<Opcode, Opcode> FEIRStmtCallAssign::InitMapOpToOpAssign() {
  std::map<Opcode, Opcode> ans;
  ans[OP_call] = OP_callassigned;
  ans[OP_virtualcall] = OP_virtualcallassigned;
  ans[OP_superclasscall] = OP_superclasscallassigned;
  ans[OP_interfacecall] = OP_interfacecallassigned;
  return ans;
}

void FEIRStmtCallAssign::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  for (const UniqueFEIRExpr &exprArg : exprArgs) {
    exprArg->RegisterDFGNodes2CheckPoint(checkPoint);
  }
  if (!methodInfo.IsReturnVoid() && (var != nullptr)) {
    var->SetDef(true);
    checkPoint.RegisterDFGNode(var);
  }
}

bool FEIRStmtCallAssign::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  bool success = true;
  for (const UniqueFEIRExpr &exprArg : exprArgs) {
    success = success && exprArg->CalculateDefs4AllUses(checkPoint, udChain);
  }
  return success;
}

std::list<StmtNode*> FEIRStmtCallAssign::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  // If the struct is a class, we check it if external type directly.
  // If the struct is a array type, we check its baseType if external type.
  // FEUtils::GetBaseTypeName returns a class type itself or an arrayType's base type.
  std::string baseStructName = FEUtils::GetBaseTypeName(methodInfo.GetStructName());
  bool isCreate = false;
  (void)FEManager::GetTypeManager().GetOrCreateClassOrInterfaceType(
      baseStructName, false, FETypeFlag::kSrcExtern, isCreate);
  std::list<StmtNode*> ans;
  StmtNode *stmtCall = nullptr;
  // prepare and find root
  methodInfo.Prepare(mirBuilder, isStatic);
  if (methodInfo.IsJavaPolymorphicCall() || methodInfo.IsJavaDynamicCall()) {
    return GenMIRStmtsUseZeroReturn(mirBuilder);
  }
  MapleVector<BaseNode*> args(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());
  for (const UniqueFEIRExpr &exprArg : exprArgs) {
    BaseNode *node = exprArg->GenMIRNode(mirBuilder);
    args.push_back(node);
  }
  PUIdx puIdx = methodInfo.GetPuIdx();
  MIRSymbol *retVarSym = nullptr;
  if (!methodInfo.IsReturnVoid() && var != nullptr) {
    retVarSym = var->GenerateLocalMIRSymbol(mirBuilder);
  }
  stmtCall = mirBuilder.CreateStmtCallAssigned(puIdx, args, retVarSym, mirOp);
  ans.push_back(stmtCall);
  return ans;
}

std::list<StmtNode*> FEIRStmtCallAssign::GenMIRStmtsUseZeroReturn(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  if (methodInfo.IsReturnVoid()) {
    return ans;
  }
  const UniqueFEIRType &retType = methodInfo.GetReturnType();
  MIRType *mirRetType = retType->GenerateMIRTypeAuto(kSrcLangJava);
  MIRSymbol *mirRetSym = var->GenerateLocalMIRSymbol(mirBuilder);
  BaseNode *nodeZero;
  if (mirRetType->IsScalarType()) {
    switch (mirRetType->GetPrimType()) {
      case PTY_u1:
      case PTY_i8:
      case PTY_i16:
      case PTY_u16:
      case PTY_i32:
        nodeZero = mirBuilder.CreateIntConst(0, PTY_i32);
        break;
      case PTY_i64:
        nodeZero = mirBuilder.CreateIntConst(0, PTY_i64);
        break;
      case PTY_f32:
        nodeZero = mirBuilder.CreateFloatConst(0.0f);
        break;
      case PTY_f64:
        nodeZero = mirBuilder.CreateDoubleConst(0.0);
        break;
      default:
        nodeZero = mirBuilder.CreateIntConst(0, PTY_i32);
        break;
    }
  } else {
    nodeZero = mirBuilder.CreateIntConst(0, PTY_ref);
  }
  StmtNode *stmt = mirBuilder.CreateStmtDassign(mirRetSym->GetStIdx(), 0, nodeZero);
  ans.push_back(stmt);
  return ans;
}

Opcode FEIRStmtCallAssign::AdjustMIROp() const {
  if (methodInfo.IsReturnVoid()) {
    auto it = mapOpAssignToOp.find(mirOp);
    if (it != mapOpAssignToOp.end()) {
      return it->second;
    }
  } else {
    auto it = mapOpToOpAssign.find(mirOp);
    if (it != mapOpToOpAssign.end()) {
      return it->second;
    }
  }
  return mirOp;
}

std::string FEIRStmtCallAssign::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  if (var != nullptr) {
    ss << " def : " << var->GetNameRaw() << ", ";
  }
  if (exprArgs.size() > 0) {
    ss << " uses : ";
    for (const UniqueFEIRExpr &exprArg : exprArgs) {
      ss << exprArg->DumpDotString() << ", ";
    }
  }
  return ss.str();
}

// ---------- FEIRStmtIntrinsicCallAssign ----------
FEIRStmtIntrinsicCallAssign::FEIRStmtIntrinsicCallAssign(MIRIntrinsicID id, UniqueFEIRType typeIn,
                                                         UniqueFEIRVar argVarRet)
    : FEIRStmtAssign(FEIRNodeKind::kStmtIntrinsicCallAssign, std::move(argVarRet)),
      intrinsicId(id),
      type(std::move(typeIn)) {}

FEIRStmtIntrinsicCallAssign::FEIRStmtIntrinsicCallAssign(MIRIntrinsicID id, UniqueFEIRType typeIn,
                                                         UniqueFEIRVar argVarRet,
                                                         std::unique_ptr<std::list<UniqueFEIRExpr>> exprListIn)
    : FEIRStmtAssign(FEIRNodeKind::kStmtIntrinsicCallAssign, std::move(argVarRet)),
      intrinsicId(id),
      type(std::move(typeIn)),
      exprList(std::move(exprListIn)) {}

FEIRStmtIntrinsicCallAssign::FEIRStmtIntrinsicCallAssign(MIRIntrinsicID id, const std::string &funcNameIn,
                                                         const std::string &protoIN,
                                                         std::unique_ptr<std::list<UniqueFEIRVar>> argsIn)
    : FEIRStmtAssign(FEIRNodeKind::kStmtIntrinsicCallAssign, nullptr),
      intrinsicId(id),
      funcName(funcNameIn),
      proto(protoIN),
      polyArgs(std::move(argsIn)) {}
FEIRStmtIntrinsicCallAssign::FEIRStmtIntrinsicCallAssign(MIRIntrinsicID id, const std::string &funcNameIn,
                                                         const std::string &protoIN,
                                                         std::unique_ptr<std::list<UniqueFEIRVar>> argsIn,
                                                         uint32 callerClassTypeIDIn, bool isInStaticFuncIn)
    : FEIRStmtAssign(FEIRNodeKind::kStmtIntrinsicCallAssign, nullptr),
      intrinsicId(id),
      funcName(funcNameIn),
      proto(protoIN),
      polyArgs(std::move(argsIn)),
      callerClassTypeID(callerClassTypeIDIn),
      isInStaticFunc(isInStaticFuncIn) {}

FEIRStmtIntrinsicCallAssign::FEIRStmtIntrinsicCallAssign(MIRIntrinsicID id, UniqueFEIRType typeIn,
                                                         UniqueFEIRVar argVarRet, uint32 typeIDIn)
    : FEIRStmtAssign(FEIRNodeKind::kStmtIntrinsicCallAssign, std::move(argVarRet)),
      intrinsicId(id),
      type(std::move(typeIn)),
      typeID(typeIDIn) {}

std::string FEIRStmtIntrinsicCallAssign::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  return ss.str();
}

void FEIRStmtIntrinsicCallAssign::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  if ((var != nullptr) && (var.get() != nullptr)) {
    var->SetDef(true);
    checkPoint.RegisterDFGNode(var);
  }
}

std::list<StmtNode*> FEIRStmtIntrinsicCallAssign::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  StmtNode *stmtCall = nullptr;
  if (intrinsicId == INTRN_JAVA_CLINIT_CHECK) {
    MapleVector<BaseNode*> args(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());
    if (FEOptions::GetInstance().IsAOT()) {
      BaseNode *argNumExpr = mirBuilder.CreateIntConst(static_cast<int64>(typeID), PTY_i32);
      args.push_back(argNumExpr);
    }
    stmtCall =
        mirBuilder.CreateStmtIntrinsicCall(INTRN_JAVA_CLINIT_CHECK, args, type->GenerateMIRType()->GetTypeIndex());
  } else if (intrinsicId == INTRN_JAVA_FILL_NEW_ARRAY) {
    MapleVector<BaseNode*> args(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());
    if (exprList != nullptr) {
      for (const auto &expr : *exprList) {
        BaseNode *node = expr->GenMIRNode(mirBuilder);
        args.push_back(node);
      }
    }
    MIRSymbol *retVarSym = nullptr;
    if ((var != nullptr) && (var.get() != nullptr)) {
      retVarSym = var->GenerateLocalMIRSymbol(mirBuilder);
    }
    stmtCall = mirBuilder.CreateStmtIntrinsicCallAssigned(INTRN_JAVA_FILL_NEW_ARRAY, args, retVarSym,
                                                          type->GenerateMIRType(true)->GetTypeIndex());
  } else if (intrinsicId == INTRN_JAVA_POLYMORPHIC_CALL) {
    return GenMIRStmtsForInvokePolyMorphic(mirBuilder);
  }
  // other intrinsic call should be implemented
  ans.emplace_back(stmtCall);
  return ans;
}

std::list<StmtNode*> FEIRStmtIntrinsicCallAssign::GenMIRStmtsForInvokePolyMorphic(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  StmtNode *stmtCall = nullptr;
  UniqueFEIRVar tmpVar;
  bool needRetypeRet = false;
  MIRSymbol *retVarSym = nullptr;
  if ((var != nullptr) && (var.get() != nullptr)) {
    PrimType ptyRet = var->GetTypeRef().GetPrimType();
    needRetypeRet = (ptyRet == PTY_f32 || ptyRet == PTY_f64);
    tmpVar = var->Clone();
    if (ptyRet == PTY_f32) {
      tmpVar->SetType(std::make_unique<FEIRTypeDefault>(PTY_i32,
                                                        GlobalTables::GetStrTable().GetOrCreateStrIdxFromName("I")));
    } else if (ptyRet == PTY_f64) {
      tmpVar->SetType(std::make_unique<FEIRTypeDefault>(PTY_i64,
                                                        GlobalTables::GetStrTable().GetOrCreateStrIdxFromName("J")));
    }
    retVarSym = tmpVar->GenerateLocalMIRSymbol(mirBuilder);
  }
  MapleVector<BaseNode*> args(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());
  ConstructArgsForInvokePolyMorphic(mirBuilder, args);
  stmtCall = mirBuilder.CreateStmtIntrinsicCallAssigned(INTRN_JAVA_POLYMORPHIC_CALL, args, retVarSym);
  ans.emplace_back(stmtCall);
  if (needRetypeRet) {
    UniqueFEIRStmt retypeStmt = FEIRBuilder::CreateStmtRetype(var->Clone(), std::move(tmpVar));
    std::list<StmtNode*> retypeMirStmts = retypeStmt->GenMIRStmts(mirBuilder);
    for (auto elem : retypeMirStmts) {
      ans.emplace_back(elem);
    }
  }
    return ans;
}

void FEIRStmtIntrinsicCallAssign::ConstructArgsForInvokePolyMorphic(MIRBuilder &mirBuilder,
                                                                    MapleVector<BaseNode*> &intrnCallargs) const {
  MIRSymbol *funcNameVal = FEManager::GetJavaStringManager().GetLiteralVar(funcName);
  if (funcNameVal == nullptr) {
    funcNameVal = FEManager::GetJavaStringManager().CreateLiteralVar(mirBuilder, funcName, false);
  }
  BaseNode *dreadExprFuncName = mirBuilder.CreateExprAddrof(0, *funcNameVal);
  dreadExprFuncName->SetPrimType(PTY_ptr);
  intrnCallargs.push_back(dreadExprFuncName);

  MIRSymbol *protoNameVal = FEManager::GetJavaStringManager().GetLiteralVar(proto);
  if (protoNameVal == nullptr) {
    protoNameVal = FEManager::GetJavaStringManager().CreateLiteralVar(mirBuilder, proto, false);
  }
  BaseNode *dreadExprProtoName = mirBuilder.CreateExprAddrof(0, *protoNameVal);
  dreadExprProtoName->SetPrimType(PTY_ptr);
  intrnCallargs.push_back(dreadExprProtoName);

  BaseNode *argNumExpr = mirBuilder.CreateIntConst(static_cast<int64>(polyArgs->size() - 1), PTY_i32);
  intrnCallargs.push_back(argNumExpr);

  bool isAfterMethodHandle = true;
  for (const auto &arg : *polyArgs) {
    intrnCallargs.push_back(mirBuilder.CreateExprDread(*(arg->GenerateMIRSymbol(mirBuilder))));
    if (FEOptions::GetInstance().IsAOT() && isAfterMethodHandle) {
      if (isInStaticFunc) {
        intrnCallargs.push_back(mirBuilder.CreateIntConst(static_cast<int32>(callerClassTypeID), PTY_i32));
      } else {
        GStrIdx thisNameIdx = GlobalTables::GetStrTable().GetStrIdxFromName("_this");
        std::unique_ptr<FEIRVar> varThisAsLocalVar = std::make_unique<FEIRVarName>(thisNameIdx,
                                                                                   FEIRTypeDefault(PTY_ref).Clone());
        intrnCallargs.push_back(mirBuilder.CreateExprDread(*(varThisAsLocalVar->GenerateMIRSymbol(mirBuilder))));
      }
      isAfterMethodHandle = false;
    }
  }
}

// ---------- FEIRExpr ----------
FEIRExpr::FEIRExpr(FEIRNodeKind argKind)
    : kind(argKind),
      isNestable(true),
      isAddrof(false),
      hasException(false) {
  type = std::make_unique<FEIRTypeDefault>();
}

FEIRExpr::FEIRExpr(FEIRNodeKind argKind, std::unique_ptr<FEIRType> argType)
    : kind(argKind),
      isNestable(true),
      isAddrof(false),
      hasException(false) {
  SetType(std::move(argType));
}

bool FEIRExpr::IsNestableImpl() const {
  return isNestable;
}

bool FEIRExpr::IsAddrofImpl() const {
  return isAddrof;
}

bool FEIRExpr::HasExceptionImpl() const {
  return hasException;
}

std::vector<FEIRVar*> FEIRExpr::GetVarUsesImpl() const {
  return std::vector<FEIRVar*>();
}

std::string FEIRExpr::DumpDotString() const {
  std::stringstream ss;
  if (kind == FEIRNodeKind::kExprArrayLoad) {
    ss << " kExprArrayLoad: ";
  }
  std::vector<FEIRVar*> varUses = GetVarUses();
  ss << "[ ";
  for (FEIRVar *use : varUses) {
    ss << use->GetNameRaw() << ", ";
  }
  ss << "] ";
  return ss.str();
}

// ---------- FEIRExprConst ----------
FEIRExprConst::FEIRExprConst()
    : FEIRExpr(FEIRNodeKind::kExprConst) {
  ASSERT(type != nullptr, "type is nullptr");
  type->SetPrimType(PTY_i32);
  value.raw = 0;
}

FEIRExprConst::FEIRExprConst(int64 val, PrimType argType)
    : FEIRExpr(FEIRNodeKind::kExprConst) {
  ASSERT(type != nullptr, "type is nullptr");
  type->SetPrimType(argType);
  value.valueI64 = val;
  CheckRawValue2SetZero();
}

FEIRExprConst::FEIRExprConst(uint64 val, PrimType argType)
    : FEIRExpr(FEIRNodeKind::kExprConst) {
  ASSERT(type != nullptr, "type is nullptr");
  type->SetPrimType(argType);
  value.valueU64 = val;
  CheckRawValue2SetZero();
}

FEIRExprConst::FEIRExprConst(float val)
    : FEIRExpr(FEIRNodeKind::kExprConst) {
  ASSERT(type != nullptr, "type is nullptr");
  type->SetPrimType(PTY_f32);
  value.valueF32 = val;
  CheckRawValue2SetZero();
}

FEIRExprConst::FEIRExprConst(double val)
    : FEIRExpr(FEIRNodeKind::kExprConst) {
  ASSERT(type != nullptr, "type is nullptr");
  type->SetPrimType(PTY_f64);
  value.valueF64 = val;
  CheckRawValue2SetZero();
}

std::unique_ptr<FEIRExpr> FEIRExprConst::CloneImpl() const {
  std::unique_ptr<FEIRExpr> expr = std::make_unique<FEIRExprConst>();
  FEIRExprConst *exprConst = static_cast<FEIRExprConst*>(expr.get());
  exprConst->value.raw = value.raw;
  ASSERT(type != nullptr, "type is nullptr");
  exprConst->type->SetPrimType(type->GetPrimType());
  exprConst->CheckRawValue2SetZero();
  return expr;
}

BaseNode *FEIRExprConst::GenMIRNodeImpl(MIRBuilder &mirBuilder) const {
  PrimType primType = GetPrimType();
  switch (primType) {
    case PTY_u1:
    case PTY_u8:
    case PTY_u16:
    case PTY_u32:
    case PTY_u64:
    case PTY_i8:
    case PTY_i16:
    case PTY_i32:
    case PTY_i64:
    case PTY_ref:
    case PTY_ptr:
      return mirBuilder.CreateIntConst(value.valueI64, primType);
    case PTY_f32:
      return mirBuilder.CreateFloatConst(value.valueF32);
    case PTY_f64:
      return mirBuilder.CreateDoubleConst(value.valueF64);
    default:
      ERR(kLncErr, "unsupported const kind");
      return nullptr;
  }
}

void FEIRExprConst::CheckRawValue2SetZero() {
  if (value.raw == 0) {
    type->SetZero(true);
  }
}

// ---------- FEIRExprDRead ----------
FEIRExprDRead::FEIRExprDRead(std::unique_ptr<FEIRVar> argVarSrc)
    : FEIRExpr(FEIRNodeKind::kExprDRead) {
  SetVarSrc(std::move(argVarSrc));
}

FEIRExprDRead::FEIRExprDRead(std::unique_ptr<FEIRType> argType, std::unique_ptr<FEIRVar> argVarSrc)
    : FEIRExpr(FEIRNodeKind::kExprDRead, std::move(argType)) {
  SetVarSrc(std::move(argVarSrc));
}

std::unique_ptr<FEIRExpr> FEIRExprDRead::CloneImpl() const {
  std::unique_ptr<FEIRExpr> expr = std::make_unique<FEIRExprDRead>(type->Clone(), varSrc->Clone());
  return expr;
}

BaseNode *FEIRExprDRead::GenMIRNodeImpl(MIRBuilder &mirBuilder) const {
  MIRType *type = varSrc->GetType()->GenerateMIRTypeAuto();
  MIRSymbol *symbol = varSrc->GenerateMIRSymbol(mirBuilder);
  ASSERT(type != nullptr, "type is nullptr");
  AddrofNode *node = mirBuilder.CreateExprDread(*type, *symbol);
  return node;
}

void FEIRExprDRead::SetVarSrc(std::unique_ptr<FEIRVar> argVarSrc) {
  CHECK_FATAL(argVarSrc != nullptr, "input is nullptr");
  varSrc = std::move(argVarSrc);
  SetType(varSrc->GetType()->Clone());
}

std::vector<FEIRVar*> FEIRExprDRead::GetVarUsesImpl() const {
  return std::vector<FEIRVar*>({ varSrc.get() });
}

// ---------- FEIRExprRegRead ----------
FEIRExprRegRead::FEIRExprRegRead(PrimType pty, int32 regNumIn)
    : FEIRExpr(FEIRNodeKind::kExprRegRead), prmType(pty), regNum(regNumIn) {}

std::unique_ptr<FEIRExpr> FEIRExprRegRead::CloneImpl() const {
  std::unique_ptr<FEIRExpr> expr = std::make_unique<FEIRExprRegRead>(prmType, regNum);
  return expr;
}

BaseNode *FEIRExprRegRead::GenMIRNodeImpl(MIRBuilder &mirBuilder) const {
  RegreadNode *node = mirBuilder.CreateExprRegread(prmType, regNum);
  return node;
}

void FEIRExprDRead::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  checkPoint.RegisterDFGNode(varSrc);
}

bool FEIRExprDRead::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  std::set<UniqueFEIRVar*> defs = checkPoint.CalcuDef(varSrc);
  (void)udChain.insert(std::make_pair(&varSrc, defs));
  return (defs.size() > 0);
}

// ---------- FEIRExprUnary ----------
std::map<Opcode, bool> FEIRExprUnary::mapOpNestable = FEIRExprUnary::InitMapOpNestableForExprUnary();

FEIRExprUnary::FEIRExprUnary(Opcode argOp, std::unique_ptr<FEIRExpr> argOpnd)
    : FEIRExpr(kExprUnary),
      op(argOp) {
  SetOpnd(std::move(argOpnd));
  SetExprTypeByOp();
}

FEIRExprUnary::FEIRExprUnary(std::unique_ptr<FEIRType> argType, Opcode argOp, std::unique_ptr<FEIRExpr> argOpnd)
    : FEIRExpr(kExprUnary, std::move(argType)),
      op(argOp) {
  SetOpnd(std::move(argOpnd));
  SetExprTypeByOp();
}

std::map<Opcode, bool> FEIRExprUnary::InitMapOpNestableForExprUnary() {
  std::map<Opcode, bool> ans;
  ans[OP_abs] = true;
  ans[OP_bnot] = true;
  ans[OP_lnot] = true;
  ans[OP_neg] = true;
  ans[OP_recip] = true;
  ans[OP_sqrt] = true;
  ans[OP_gcmallocjarray] = false;
  return ans;
}

std::unique_ptr<FEIRExpr> FEIRExprUnary::CloneImpl() const {
  std::unique_ptr<FEIRExpr> expr = std::make_unique<FEIRExprUnary>(type->Clone(), op, opnd->Clone());
  return expr;
}

BaseNode *FEIRExprUnary::GenMIRNodeImpl(MIRBuilder &mirBuilder) const {
  MIRType *mirType =
      GlobalTables::GetTypeTable().GetTypeFromTyIdx(TyIdx(static_cast<uint32>(GetTypeRef().GetPrimType())));
  ASSERT(mirType != nullptr, "mir type is nullptr");
  BaseNode *nodeOpnd = opnd->GenMIRNode(mirBuilder);
  BaseNode *expr = mirBuilder.CreateExprUnary(op, *mirType, nodeOpnd);
  return expr;
}

void FEIRExprUnary::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  opnd->RegisterDFGNodes2CheckPoint(checkPoint);
}

bool FEIRExprUnary::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  return opnd->CalculateDefs4AllUses(checkPoint, udChain);
}

std::vector<FEIRVar*> FEIRExprUnary::GetVarUsesImpl() const {
  return opnd->GetVarUses();
}

void FEIRExprUnary::SetOpnd(std::unique_ptr<FEIRExpr> argOpnd) {
  CHECK_FATAL(argOpnd != nullptr, "opnd is nullptr");
  opnd = std::move(argOpnd);
}

void FEIRExprUnary::SetExprTypeByOp() {
  switch (op) {
    case OP_neg:
    case OP_bnot:
      type->SetPrimType(opnd->GetPrimType());
      break;
    default:
      break;
  }
}

// ---------- FEIRExprTypeCvt ----------
std::map<Opcode, bool> FEIRExprTypeCvt::mapOpNestable = FEIRExprTypeCvt::InitMapOpNestableForTypeCvt();
std::map<Opcode, FEIRExprTypeCvt::FuncPtrGenMIRNode> FEIRExprTypeCvt::funcPtrMapForParseExpr =
    FEIRExprTypeCvt::InitFuncPtrMapForParseExpr();

FEIRExprTypeCvt::FEIRExprTypeCvt(Opcode argOp, std::unique_ptr<FEIRExpr> argOpnd)
    : FEIRExprUnary(argOp, std::move(argOpnd)) {}

FEIRExprTypeCvt::FEIRExprTypeCvt(std::unique_ptr<FEIRType> exprType, Opcode argOp, std::unique_ptr<FEIRExpr> argOpnd)
    : FEIRExprUnary(std::move(exprType), argOp, std::move(argOpnd)) {}

std::unique_ptr<FEIRExpr> FEIRExprTypeCvt::CloneImpl() const {
  std::unique_ptr<FEIRExpr> expr = std::make_unique<FEIRExprTypeCvt>(type->Clone(), op, opnd->Clone());
  return expr;
}

BaseNode *FEIRExprTypeCvt::GenMIRNodeImpl(MIRBuilder &mirBuilder) const {
  auto ptrFunc = funcPtrMapForParseExpr.find(op);
  ASSERT(ptrFunc != funcPtrMapForParseExpr.end(), "unsupported op: %s", kOpcodeInfo.GetName(op).c_str());
  return (this->*(ptrFunc->second))(mirBuilder);
}

void FEIRExprTypeCvt::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  opnd->RegisterDFGNodes2CheckPoint(checkPoint);
}

bool FEIRExprTypeCvt::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  return opnd->CalculateDefs4AllUses(checkPoint, udChain);
}

Opcode FEIRExprTypeCvt::ChooseOpcodeByFromVarAndToVar(const FEIRVar &fromVar, const FEIRVar &toVar) {
  if ((fromVar.GetType()->IsRef()) && (toVar.GetType()->IsRef())) {
    return OP_retype;
  }
  return OP_retype;
}

BaseNode *FEIRExprTypeCvt::GenMIRNodeMode1(MIRBuilder &mirBuilder) const {
  // MIR: op <to-type> <from-type> (<opnd0>)
  MIRType *mirTypeDst =
      GlobalTables::GetTypeTable().GetTypeFromTyIdx(TyIdx(static_cast<uint32>(GetTypeRef().GetPrimType())));
  MIRType *mirTypeSrc = GlobalTables::GetTypeTable().GetTypeFromTyIdx(TyIdx(static_cast<uint32>(opnd->GetPrimType())));
  BaseNode *nodeOpnd = opnd->GenMIRNode(mirBuilder);
  BaseNode *expr = mirBuilder.CreateExprTypeCvt(op, *mirTypeDst, *mirTypeSrc, nodeOpnd);
  return expr;
}

BaseNode *FEIRExprTypeCvt::GenMIRNodeMode2(MIRBuilder &mirBuilder) const {
  // MIR: op <prim-type> <float-type> (<opnd0>)
  PrimType primTypeSrc = opnd->GetPrimType();
  CHECK_FATAL(IsPrimitiveFloat(primTypeSrc), "from type must be float type");
  return GenMIRNodeMode1(mirBuilder);
}

BaseNode *FEIRExprTypeCvt::GenMIRNodeMode3(MIRBuilder &mirBuilder) const {
  // MIR: retype <prim-type> <type> (<opnd0>)
  MIRType *mirTypeDst = GetTypeRef().GenerateMIRType();
  MIRType *mirTypeSrc = opnd->GetTypeRef().GenerateMIRTypeAuto();
  BaseNode *nodeOpnd = opnd->GenMIRNode(mirBuilder);
  BaseNode *expr = mirBuilder.CreateExprRetype(*mirTypeDst, *mirTypeSrc, nodeOpnd);
  return expr;
}

std::map<Opcode, bool> FEIRExprTypeCvt::InitMapOpNestableForTypeCvt() {
  std::map<Opcode, bool> ans;
  ans[OP_ceil] = true;
  ans[OP_cvt] = true;
  ans[OP_floor] = true;
  ans[OP_retype] = true;
  ans[OP_round] = true;
  ans[OP_trunc] = true;
  return ans;
}

std::map<Opcode, FEIRExprTypeCvt::FuncPtrGenMIRNode> FEIRExprTypeCvt::InitFuncPtrMapForParseExpr() {
  std::map<Opcode, FuncPtrGenMIRNode> ans;
  ans[OP_ceil] = &FEIRExprTypeCvt::GenMIRNodeMode2;
  ans[OP_cvt] = &FEIRExprTypeCvt::GenMIRNodeMode1;
  ans[OP_floor] = &FEIRExprTypeCvt::GenMIRNodeMode2;
  ans[OP_retype] = &FEIRExprTypeCvt::GenMIRNodeMode3;
  ans[OP_round] = &FEIRExprTypeCvt::GenMIRNodeMode2;
  ans[OP_trunc] = &FEIRExprTypeCvt::GenMIRNodeMode2;
  return ans;
}

// ---------- FEIRExprExtractBits ----------
std::map<Opcode, bool> FEIRExprExtractBits::mapOpNestable = FEIRExprExtractBits::InitMapOpNestableForExtractBits();
std::map<Opcode, FEIRExprExtractBits::FuncPtrGenMIRNode> FEIRExprExtractBits::funcPtrMapForParseExpr =
    FEIRExprExtractBits::InitFuncPtrMapForParseExpr();

FEIRExprExtractBits::FEIRExprExtractBits(Opcode argOp, PrimType argPrimType, uint8 argBitOffset, uint8 argBitSize,
                                         std::unique_ptr<FEIRExpr> argOpnd)
    : FEIRExprUnary(argOp, std::move(argOpnd)),
      bitOffset(argBitOffset),
      bitSize(argBitSize) {
  CHECK_FATAL(IsPrimitiveInteger(argPrimType), "only integer type is supported");
  type->SetPrimType(argPrimType);
}

FEIRExprExtractBits::FEIRExprExtractBits(Opcode argOp, PrimType argPrimType, std::unique_ptr<FEIRExpr> argOpnd)
    : FEIRExprExtractBits(argOp, argPrimType, 0, 0, std::move(argOpnd)) {}

std::unique_ptr<FEIRExpr> FEIRExprExtractBits::CloneImpl() const {
  std::unique_ptr<FEIRExpr> expr = std::make_unique<FEIRExprExtractBits>(op, type->GetPrimType(), bitOffset, bitSize,
                                                                         opnd->Clone());
  return expr;
}

BaseNode *FEIRExprExtractBits::GenMIRNodeImpl(MIRBuilder &mirBuilder) const {
  auto ptrFunc = funcPtrMapForParseExpr.find(op);
  ASSERT(ptrFunc != funcPtrMapForParseExpr.end(), "unsupported op: %s", kOpcodeInfo.GetName(op).c_str());
  return (this->*(ptrFunc->second))(mirBuilder);
}

void FEIRExprExtractBits::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  opnd->RegisterDFGNodes2CheckPoint(checkPoint);
}

bool FEIRExprExtractBits::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  return opnd->CalculateDefs4AllUses(checkPoint, udChain);
}

std::map<Opcode, bool> FEIRExprExtractBits::InitMapOpNestableForExtractBits() {
  std::map<Opcode, bool> ans;
  ans[OP_extractbits] = true;
  ans[OP_sext] = true;
  ans[OP_zext] = true;
  return ans;
}

std::map<Opcode, FEIRExprExtractBits::FuncPtrGenMIRNode> FEIRExprExtractBits::InitFuncPtrMapForParseExpr() {
  std::map<Opcode, FuncPtrGenMIRNode> ans;
  ans[OP_extractbits] = &FEIRExprExtractBits::GenMIRNodeForExtrabits;
  ans[OP_sext] = &FEIRExprExtractBits::GenMIRNodeForExt;
  ans[OP_zext] = &FEIRExprExtractBits::GenMIRNodeForExt;
  return ans;
}

BaseNode *FEIRExprExtractBits::GenMIRNodeForExtrabits(MIRBuilder &mirBuilder) const {
  ASSERT(opnd != nullptr, "nullptr check");
  PrimType primTypeDst = GetTypeRef().GetPrimType();
  PrimType primTypeSrc = opnd->GetPrimType();
  CHECK_FATAL(FEUtils::IsInteger(primTypeDst), "dst type of extrabits must integer");
  CHECK_FATAL(FEUtils::IsInteger(primTypeSrc), "src type of extrabits must integer");
  uint8 widthDst = FEUtils::GetWidth(primTypeDst);
  uint8 widthSrc = FEUtils::GetWidth(primTypeSrc);
  CHECK_FATAL(widthDst >= bitSize, "dst width is not enough");
  CHECK_FATAL(widthSrc >= bitOffset + bitSize, "src width is not enough");
  MIRType *mirTypeDst = GetTypeRef().GenerateMIRTypeAuto();
  BaseNode *nodeOpnd = opnd->GenMIRNode(mirBuilder);
  BaseNode *expr = mirBuilder.CreateExprExtractbits(op, *mirTypeDst, bitOffset, bitSize, nodeOpnd);
  return expr;
}

BaseNode *FEIRExprExtractBits::GenMIRNodeForExt(MIRBuilder &mirBuilder) const {
  ASSERT(opnd != nullptr, "nullptr check");
  PrimType primTypeDst = GetTypeRef().GetPrimType();
  CHECK_FATAL(FEUtils::IsInteger(primTypeDst), "dst type of sext/zext must integer");
  uint8 widthDst = FEUtils::GetWidth(primTypeDst);
  MIRType *mirTypeDst = GetTypeRef().GenerateMIRTypeAuto();
  BaseNode *nodeOpnd = opnd->GenMIRNode(mirBuilder);
  BaseNode *expr = mirBuilder.CreateExprExtractbits(op, *mirTypeDst, 0, widthDst, nodeOpnd);
  return expr;
}

// ---------- FEIRExprBinary ----------
std::map<Opcode, FEIRExprBinary::FuncPtrGenMIRNode> FEIRExprBinary::funcPtrMapForGenMIRNode =
    FEIRExprBinary::InitFuncPtrMapForGenMIRNode();

FEIRExprBinary::FEIRExprBinary(Opcode argOp, std::unique_ptr<FEIRExpr> argOpnd0, std::unique_ptr<FEIRExpr> argOpnd1)
    : FEIRExpr(FEIRNodeKind::kExprBinary),
      op(argOp) {
  SetOpnd0(std::move(argOpnd0));
  SetOpnd1(std::move(argOpnd1));
  SetExprTypeByOp();
}

FEIRExprBinary::FEIRExprBinary(std::unique_ptr<FEIRType> exprType, Opcode argOp, std::unique_ptr<FEIRExpr> argOpnd0,
                               std::unique_ptr<FEIRExpr> argOpnd1)
    : FEIRExpr(FEIRNodeKind::kExprBinary, std::move(exprType)),
      op(argOp) {
  SetOpnd0(std::move(argOpnd0));
  SetOpnd1(std::move(argOpnd1));
  SetExprTypeByOp();
}

std::unique_ptr<FEIRExpr> FEIRExprBinary::CloneImpl() const {
  std::unique_ptr<FEIRExpr> expr = std::make_unique<FEIRExprBinary>(type->Clone(), op, opnd0->Clone(), opnd1->Clone());
  return expr;
}

BaseNode *FEIRExprBinary::GenMIRNodeImpl(MIRBuilder &mirBuilder) const {
  auto ptrFunc = funcPtrMapForGenMIRNode.find(op);
  ASSERT(ptrFunc != funcPtrMapForGenMIRNode.end(), "unsupported op: %s", kOpcodeInfo.GetName(op).c_str());
  return (this->*(ptrFunc->second))(mirBuilder);
}

void FEIRExprBinary::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  opnd0->RegisterDFGNodes2CheckPoint(checkPoint);
  opnd1->RegisterDFGNodes2CheckPoint(checkPoint);
}

bool FEIRExprBinary::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  bool success = true;
  success = success && opnd0->CalculateDefs4AllUses(checkPoint, udChain);
  success = success && opnd1->CalculateDefs4AllUses(checkPoint, udChain);
  return success;
}

std::vector<FEIRVar*> FEIRExprBinary::GetVarUsesImpl() const {
  std::vector<FEIRVar*> ans;
  for (FEIRVar *var : opnd0->GetVarUses()) {
    ans.push_back(var);
  }
  for (FEIRVar *var : opnd1->GetVarUses()) {
    ans.push_back(var);
  }
  return ans;
}

bool FEIRExprBinary::IsNestableImpl() const {
  return true;
}

bool FEIRExprBinary::IsAddrofImpl() const {
  return false;
}

void FEIRExprBinary::SetOpnd0(std::unique_ptr<FEIRExpr> argOpnd) {
  CHECK_FATAL(argOpnd != nullptr, "input is nullptr");
  opnd0 = std::move(argOpnd);
}

void FEIRExprBinary::SetOpnd1(std::unique_ptr<FEIRExpr> argOpnd) {
  CHECK_FATAL(argOpnd != nullptr, "input is nullptr");
  opnd1 = std::move(argOpnd);
}

std::map<Opcode, FEIRExprBinary::FuncPtrGenMIRNode> FEIRExprBinary::InitFuncPtrMapForGenMIRNode() {
  std::map<Opcode, FEIRExprBinary::FuncPtrGenMIRNode> ans;
  ans[OP_add] = &FEIRExprBinary::GenMIRNodeNormal;
  ans[OP_ashr] = &FEIRExprBinary::GenMIRNodeNormal;
  ans[OP_band] = &FEIRExprBinary::GenMIRNodeNormal;
  ans[OP_bior] = &FEIRExprBinary::GenMIRNodeNormal;
  ans[OP_bxor] = &FEIRExprBinary::GenMIRNodeNormal;
  ans[OP_cand] = &FEIRExprBinary::GenMIRNodeNormal;
  ans[OP_cior] = &FEIRExprBinary::GenMIRNodeNormal;
  ans[OP_cmp] = &FEIRExprBinary::GenMIRNodeCompare;
  ans[OP_cmpg] = &FEIRExprBinary::GenMIRNodeCompare;
  ans[OP_cmpl] = &FEIRExprBinary::GenMIRNodeCompare;
  ans[OP_div] = &FEIRExprBinary::GenMIRNodeNormal;
  ans[OP_eq] = &FEIRExprBinary::GenMIRNodeCompareU1;
  ans[OP_ge] = &FEIRExprBinary::GenMIRNodeCompareU1;
  ans[OP_gt] = &FEIRExprBinary::GenMIRNodeCompareU1;
  ans[OP_land] = &FEIRExprBinary::GenMIRNodeNormal;
  ans[OP_lior] = &FEIRExprBinary::GenMIRNodeNormal;
  ans[OP_le] = &FEIRExprBinary::GenMIRNodeCompareU1;
  ans[OP_lshr] = &FEIRExprBinary::GenMIRNodeNormal;
  ans[OP_lt] = &FEIRExprBinary::GenMIRNodeCompareU1;
  ans[OP_max] = &FEIRExprBinary::GenMIRNodeNormal;
  ans[OP_min] = &FEIRExprBinary::GenMIRNodeNormal;
  ans[OP_mul] = &FEIRExprBinary::GenMIRNodeNormal;
  ans[OP_ne] = &FEIRExprBinary::GenMIRNodeCompareU1;
  ans[OP_rem] = &FEIRExprBinary::GenMIRNodeNormal;
  ans[OP_shl] = &FEIRExprBinary::GenMIRNodeNormal;
  ans[OP_sub] = &FEIRExprBinary::GenMIRNodeNormal;
  return ans;
}

BaseNode *FEIRExprBinary::GenMIRNodeNormal(MIRBuilder &mirBuilder) const {
  MIRType *mirTypeDst = GlobalTables::GetTypeTable().GetTypeFromTyIdx(TyIdx(static_cast<uint32>(type->GetPrimType())));
  BaseNode *nodeOpnd0 = opnd0->GenMIRNode(mirBuilder);
  BaseNode *nodeOpnd1 = opnd1->GenMIRNode(mirBuilder);
  BaseNode *expr = mirBuilder.CreateExprBinary(op, *mirTypeDst, nodeOpnd0, nodeOpnd1);
  return expr;
}

BaseNode *FEIRExprBinary::GenMIRNodeCompare(MIRBuilder &mirBuilder) const {
  PrimType primTypeOpnd0 = opnd0->GetPrimType();
  PrimType primTypeOpnd1 = opnd1->GetPrimType();
  CHECK_FATAL(primTypeOpnd0 == primTypeOpnd1, "primtype of opnds must be the same");
  MIRType *mirTypeDst = GlobalTables::GetTypeTable().GetTypeFromTyIdx(TyIdx(static_cast<uint32>(type->GetPrimType())));
  MIRType *mirTypeSrc = GlobalTables::GetTypeTable().GetTypeFromTyIdx(TyIdx(primTypeOpnd0));
  BaseNode *nodeOpnd0 = opnd0->GenMIRNode(mirBuilder);
  BaseNode *nodeOpnd1 = opnd1->GenMIRNode(mirBuilder);
  BaseNode *expr = mirBuilder.CreateExprCompare(op, *mirTypeDst, *mirTypeSrc, nodeOpnd0, nodeOpnd1);
  return expr;
}

BaseNode *FEIRExprBinary::GenMIRNodeCompareU1(MIRBuilder &mirBuilder) const {
  PrimType primTypeOpnd0 = opnd0->GetPrimType();
  PrimType primTypeOpnd1 = opnd1->GetPrimType();
  CHECK_FATAL(primTypeOpnd0 == primTypeOpnd1, "primtype of opnds must be the same");
  MIRType *mirTypeSrc = GlobalTables::GetTypeTable().GetTypeFromTyIdx(TyIdx(static_cast<uint32>(primTypeOpnd0)));
  BaseNode *nodeOpnd0 = opnd0->GenMIRNode(mirBuilder);
  BaseNode *nodeOpnd1 = opnd1->GenMIRNode(mirBuilder);
  MIRType *mirTypeU1 = GlobalTables::GetTypeTable().GetUInt1();
  BaseNode *expr = mirBuilder.CreateExprCompare(op, *mirTypeU1, *mirTypeSrc, nodeOpnd0, nodeOpnd1);
  return expr;
}

void FEIRExprBinary::SetExprTypeByOp() {
  switch (op) {
    // Normal
    case OP_add:
    case OP_div:
    case OP_max:
    case OP_min:
    case OP_mul:
    case OP_rem:
    case OP_sub:
      SetExprTypeByOpNormal();
      break;
    // Shift
    case OP_ashr:
    case OP_lshr:
    case OP_shl:
      SetExprTypeByOpShift();
      break;
    // Logic
    case OP_band:
    case OP_bior:
    case OP_bxor:
    case OP_cand:
    case OP_cior:
    case OP_land:
    case OP_lior:
      SetExprTypeByOpLogic();
      break;
    // Compare
    case OP_cmp:
    case OP_cmpl:
    case OP_cmpg:
    case OP_eq:
    case OP_ge:
    case OP_gt:
    case OP_le:
    case OP_lt:
    case OP_ne:
      SetExprTypeByOpCompare();
      break;
    default:
      break;
  }
}

void FEIRExprBinary::SetExprTypeByOpNormal() {
  PrimType primTypeOpnd0 = opnd0->GetPrimType();
  PrimType primTypeOpnd1 = opnd1->GetPrimType();
  CHECK_FATAL(primTypeOpnd0 == primTypeOpnd1, "primtype of opnds must be the same");
  type->SetPrimType(primTypeOpnd0);
}

void FEIRExprBinary::SetExprTypeByOpShift() {
  PrimType primTypeOpnd0 = opnd0->GetPrimType();
  PrimType primTypeOpnd1 = opnd1->GetPrimType();
  CHECK_FATAL(IsPrimitiveInteger(primTypeOpnd0), "logic's opnd0 must be integer");
  CHECK_FATAL(IsPrimitiveInteger(primTypeOpnd1), "logic's opnd1 must be integer");
  type->SetPrimType(primTypeOpnd0);
}

void FEIRExprBinary::SetExprTypeByOpLogic() {
  PrimType primTypeOpnd0 = opnd0->GetPrimType();
  PrimType primTypeOpnd1 = opnd1->GetPrimType();
  CHECK_FATAL(primTypeOpnd0 == primTypeOpnd1, "primtype of opnds must be the same");
  CHECK_FATAL(IsPrimitiveInteger(primTypeOpnd0), "logic's opnds must be integer");
  type->SetPrimType(primTypeOpnd0);
}

void FEIRExprBinary::SetExprTypeByOpCompare() {
  PrimType primTypeOpnd0 = opnd0->GetPrimType();
  PrimType primTypeOpnd1 = opnd1->GetPrimType();
  CHECK_FATAL(primTypeOpnd0 == primTypeOpnd1 ||
              (opnd0->GetKind() == kExprConst && static_cast<FEIRExprConst*>(opnd0.get())->GetValueRaw() == 0) ||
              (opnd1->GetKind() == kExprConst && static_cast<FEIRExprConst*>(opnd1.get())->GetValueRaw() == 0),
              "primtype of opnds must be the same");
  type->SetPrimType(PTY_i32);
}

// ---------- FEIRExprTernary ----------
FEIRExprTernary::FEIRExprTernary(Opcode argOp, std::unique_ptr<FEIRExpr> argOpnd0, std::unique_ptr<FEIRExpr> argOpnd1,
                                 std::unique_ptr<FEIRExpr> argOpnd2)
    : FEIRExpr(FEIRNodeKind::kExprTernary),
      op(argOp) {
  SetOpnd(std::move(argOpnd0), 0);
  SetOpnd(std::move(argOpnd1), 1);
  SetOpnd(std::move(argOpnd2), 2);
  SetExprTypeByOp();
}

std::unique_ptr<FEIRExpr> FEIRExprTernary::CloneImpl() const {
  std::unique_ptr<FEIRExpr> expr = std::make_unique<FEIRExprTernary>(op, opnd0->Clone(), opnd1->Clone(),
                                                                     opnd2->Clone());
  return expr;
}

BaseNode *FEIRExprTernary::GenMIRNodeImpl(MIRBuilder &mirBuilder) const {
  MIRType *mirTypeDst = GlobalTables::GetTypeTable().GetTypeFromTyIdx(TyIdx(static_cast<uint32>(type->GetPrimType())));
  BaseNode *nodeOpnd0 = opnd0->GenMIRNode(mirBuilder);
  BaseNode *nodeOpnd1 = opnd1->GenMIRNode(mirBuilder);
  BaseNode *nodeOpnd2 = opnd2->GenMIRNode(mirBuilder);
  BaseNode *expr = mirBuilder.CreateExprTernary(op, *mirTypeDst, nodeOpnd0, nodeOpnd1, nodeOpnd2);
  return expr;
}

void FEIRExprTernary::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  opnd0->RegisterDFGNodes2CheckPoint(checkPoint);
  opnd1->RegisterDFGNodes2CheckPoint(checkPoint);
  opnd2->RegisterDFGNodes2CheckPoint(checkPoint);
}

bool FEIRExprTernary::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  bool success = true;
  success = success && opnd0->CalculateDefs4AllUses(checkPoint, udChain);
  success = success && opnd1->CalculateDefs4AllUses(checkPoint, udChain);
  success = success && opnd2->CalculateDefs4AllUses(checkPoint, udChain);
  return success;
}

std::vector<FEIRVar*> FEIRExprTernary::GetVarUsesImpl() const {
  std::vector<FEIRVar*> ans;
  for (FEIRVar *var : opnd0->GetVarUses()) {
    ans.push_back(var);
  }
  for (FEIRVar *var : opnd1->GetVarUses()) {
    ans.push_back(var);
  }
  for (FEIRVar *var : opnd2->GetVarUses()) {
    ans.push_back(var);
  }
  return ans;
}

bool FEIRExprTernary::IsNestableImpl() const {
  return true;
}

bool FEIRExprTernary::IsAddrofImpl() const {
  return false;
}

void FEIRExprTernary::SetOpnd(std::unique_ptr<FEIRExpr> argOpnd, uint32 idx) {
  CHECK_FATAL(argOpnd != nullptr, "input is nullptr");
  switch (idx) {
    case 0:
      opnd0 = std::move(argOpnd);
      break;
    case 1:
      opnd1 = std::move(argOpnd);
      break;
    case 2:
      opnd2 = std::move(argOpnd);
      break;
    default:
      CHECK_FATAL(false, "index out of range");
  }
}

void FEIRExprTernary::SetExprTypeByOp() {
  PrimType primTypeOpnd1 = opnd1->GetPrimType();
  PrimType primTypeOpnd2 = opnd2->GetPrimType();
  CHECK_FATAL(primTypeOpnd1 == primTypeOpnd2, "primtype of opnds must be the same");
  type->SetPrimType(primTypeOpnd1);
}

// ---------- FEIRExprNary ----------
FEIRExprNary::FEIRExprNary(Opcode argOp)
    : FEIRExpr(FEIRNodeKind::kExprNary),
      op(argOp) {}

std::vector<FEIRVar*> FEIRExprNary::GetVarUsesImpl() const {
  std::vector<FEIRVar*> ans;
  for (const std::unique_ptr<FEIRExpr> &opnd : opnds) {
    for (FEIRVar *var : opnd->GetVarUses()) {
      ans.push_back(var);
    }
  }
  return ans;
}

void FEIRExprNary::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  for (const std::unique_ptr<FEIRExpr> &opnd : opnds) {
    opnd->RegisterDFGNodes2CheckPoint(checkPoint);
  }
}

bool FEIRExprNary::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  bool success = true;
  for (const std::unique_ptr<FEIRExpr> &opnd : opnds) {
    success = success && opnd->CalculateDefs4AllUses(checkPoint, udChain);
  }
  return success;
}

void FEIRExprNary::AddOpnd(std::unique_ptr<FEIRExpr> argOpnd) {
  CHECK_FATAL(argOpnd != nullptr, "input opnd is nullptr");
  opnds.push_back(std::move(argOpnd));
}

void FEIRExprNary::AddOpnds(const std::vector<std::unique_ptr<FEIRExpr>> &argOpnds) {
  for (const std::unique_ptr<FEIRExpr> &opnd : argOpnds) {
    ASSERT_NOT_NULL(opnd);
    AddOpnd(opnd->Clone());
  }
}

void FEIRExprNary::ResetOpnd() {
  opnds.clear();
}

// ---------- FEIRExprIntrinsicop ----------
FEIRExprIntrinsicop::FEIRExprIntrinsicop(std::unique_ptr<FEIRType> exprType, MIRIntrinsicID argIntrinsicID)
    : FEIRExprNary(OP_intrinsicop),
      intrinsicID(argIntrinsicID) {
  kind = FEIRNodeKind::kExprIntrinsicop;
  SetType(std::move(exprType));
}

FEIRExprIntrinsicop::FEIRExprIntrinsicop(std::unique_ptr<FEIRType> exprType, MIRIntrinsicID argIntrinsicID,
                                         std::unique_ptr<FEIRType> argParamType)
    : FEIRExprNary(OP_intrinsicopwithtype),
      intrinsicID(argIntrinsicID) {
  kind = FEIRNodeKind::kExprIntrinsicop;
  SetType(std::move(exprType));
  paramType = std::move(argParamType);
}

FEIRExprIntrinsicop::FEIRExprIntrinsicop(std::unique_ptr<FEIRType> exprType, MIRIntrinsicID argIntrinsicID,
                                         const std::vector<std::unique_ptr<FEIRExpr>> &argOpnds)
    : FEIRExprIntrinsicop(std::move(exprType), argIntrinsicID) {
  AddOpnds(argOpnds);
}

FEIRExprIntrinsicop::FEIRExprIntrinsicop(std::unique_ptr<FEIRType> exprType, MIRIntrinsicID argIntrinsicID,
                                         std::unique_ptr<FEIRType> argParamType,
                                         const std::vector<std::unique_ptr<FEIRExpr>> &argOpnds)
    : FEIRExprIntrinsicop(std::move(exprType), argIntrinsicID, std::move(argParamType)) {
  AddOpnds(argOpnds);
}

FEIRExprIntrinsicop::FEIRExprIntrinsicop(std::unique_ptr<FEIRType> exprType, MIRIntrinsicID argIntrinsicID,
                                         std::unique_ptr<FEIRType> argParamType, uint32 argTypeID)
    : FEIRExprNary(OP_intrinsicopwithtype),
      intrinsicID(argIntrinsicID),
      typeID(argTypeID) {
  kind = FEIRNodeKind::kExprIntrinsicop;
  SetType(std::move(exprType));
  paramType = std::move(argParamType);
}

FEIRExprIntrinsicop::FEIRExprIntrinsicop(std::unique_ptr<FEIRType> exprType, MIRIntrinsicID argIntrinsicID,
                                         std::unique_ptr<FEIRType> argParamType, uint32 argTypeID, uint32 argArraySize)
    : FEIRExprNary(OP_intrinsicopwithtype),
      intrinsicID(argIntrinsicID),
      typeID(argTypeID),
      arraySize(argArraySize) {
  kind = FEIRNodeKind::kExprIntrinsicop;
  SetType(std::move(exprType));
  paramType = std::move(argParamType);
}

std::unique_ptr<FEIRExpr> FEIRExprIntrinsicop::CloneImpl() const {
  if (op == OP_intrinsicop) {
    return std::make_unique<FEIRExprIntrinsicop>(type->Clone(), intrinsicID, opnds);
  } else {
    CHECK_FATAL(paramType != nullptr, "error: param type is not set");
    return std::make_unique<FEIRExprIntrinsicop>(type->Clone(), intrinsicID, paramType->Clone(), opnds);
  }
}

BaseNode *FEIRExprIntrinsicop::GenMIRNodeImpl(MIRBuilder &mirBuilder) const {
  MapleVector<BaseNode*> args(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());
  for (const auto &e : opnds) {
    BaseNode *node = e->GenMIRNode(mirBuilder);
    args.emplace_back(node);
  }
  (void)arraySize;
  (void)typeID;
  MIRType *ptrType = GlobalTables::GetTypeTable().GetOrCreatePointerType(
      *(type->GenerateMIRType()), paramType->IsRef() ? PTY_ref : PTY_ptr);
  BaseNode *expr = mirBuilder.CreateExprIntrinsicop(intrinsicID, op, *ptrType, args);
  return expr;
}

void FEIRExprIntrinsicop::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  for (const std::unique_ptr<FEIRExpr> &opnd : opnds) {
    opnd->RegisterDFGNodes2CheckPoint(checkPoint);
  }
}

bool FEIRExprIntrinsicop::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  bool success = true;
  for (const std::unique_ptr<FEIRExpr> &opnd : opnds) {
    success = success && opnd->CalculateDefs4AllUses(checkPoint, udChain);
  }
  return success;
}

bool FEIRExprIntrinsicop::IsNestableImpl() const {
  return false;
}

bool FEIRExprIntrinsicop::IsAddrofImpl() const {
  return false;
}

// ---------- FEIRExprJavaMerge ----------------
FEIRExprJavaMerge::FEIRExprJavaMerge(std::unique_ptr<FEIRType> mergedTypeArg,
                                     const std::vector<std::unique_ptr<FEIRExpr>> &argOpnds)
    : FEIRExprNary(OP_intrinsicop) {
  SetType(std::move(mergedTypeArg));
  AddOpnds(argOpnds);
}

std::unique_ptr<FEIRExpr> FEIRExprJavaMerge::CloneImpl() const {
  return std::make_unique<FEIRExprJavaMerge>(type->Clone(), opnds);
}

void FEIRExprJavaMerge::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  for (const std::unique_ptr<FEIRExpr> &opnd : opnds) {
    opnd->RegisterDFGNodes2CheckPoint(checkPoint);
  }
}

bool FEIRExprJavaMerge::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  bool success = true;
  for (const std::unique_ptr<FEIRExpr> &opnd : opnds) {
    success = success && opnd->CalculateDefs4AllUses(checkPoint, udChain);
  }
  return success;
}

BaseNode *FEIRExprJavaMerge::GenMIRNodeImpl(MIRBuilder &mirBuilder) const {
  MapleVector<BaseNode*> args(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());
  for (const auto &e : opnds) {
    BaseNode *node = e->GenMIRNode(mirBuilder);
    args.emplace_back(node);
  }
  // (intrinsicop u1 JAVA_MERGE (dread i32 %Reg0_I))
  IntrinDesc *intrinDesc = &IntrinDesc::intrinTable[INTRN_JAVA_MERGE];
  MIRType *retType = intrinDesc->GetReturnType();
  BaseNode *intr = mirBuilder.CreateExprIntrinsicop(INTRN_JAVA_MERGE, op, *retType, args);
  intr->SetPrimType(type->GetPrimType());
  return intr;
}

// ---------- FEIRExprJavaNewInstance ----------
FEIRExprJavaNewInstance::FEIRExprJavaNewInstance(UniqueFEIRType argType)
    : FEIRExpr(FEIRNodeKind::kExprJavaNewInstance) {
  SetType(std::move(argType));
}

FEIRExprJavaNewInstance::FEIRExprJavaNewInstance(UniqueFEIRType argType, uint32 argTypeID)
    : FEIRExpr(FEIRNodeKind::kExprJavaNewInstance), typeID(argTypeID) {
  SetType(std::move(argType));
}

std::unique_ptr<FEIRExpr> FEIRExprJavaNewInstance::CloneImpl() const {
  std::unique_ptr<FEIRExpr> expr = std::make_unique<FEIRExprJavaNewInstance>(type->Clone());
  CHECK_NULL_FATAL(expr);
  return expr;
}

BaseNode *FEIRExprJavaNewInstance::GenMIRNodeImpl(MIRBuilder &mirBuilder) const {
  MIRType *mirType = type->GenerateMIRType(kSrcLangJava, false);
  BaseNode *expr = nullptr;
  (void)typeID;
  MIRType *ptrType = GlobalTables::GetTypeTable().GetOrCreatePointerType(*mirType, PTY_ref);
  expr = mirBuilder.CreateExprGCMalloc(OP_gcmalloc, *ptrType, *mirType);
  CHECK_NULL_FATAL(expr);
  return expr;
}

// ---------- FEIRExprJavaNewArray ----------
FEIRExprJavaNewArray::FEIRExprJavaNewArray(UniqueFEIRType argArrayType, UniqueFEIRExpr argExprSize)
    : FEIRExpr(FEIRNodeKind::kExprJavaNewArray) {
  SetArrayType(std::move(argArrayType));
  SetExprSize(std::move(argExprSize));
}

FEIRExprJavaNewArray::FEIRExprJavaNewArray(UniqueFEIRType argArrayType, UniqueFEIRExpr argExprSize, uint32 argTypeID)
    : FEIRExpr(FEIRNodeKind::kExprJavaNewArray), typeID(argTypeID) {
  SetArrayType(std::move(argArrayType));
  SetExprSize(std::move(argExprSize));
}

std::unique_ptr<FEIRExpr> FEIRExprJavaNewArray::CloneImpl() const {
  std::unique_ptr<FEIRExpr> expr = std::make_unique<FEIRExprJavaNewArray>(arrayType->Clone(), exprSize->Clone());
  CHECK_NULL_FATAL(expr);
  return expr;
}

std::vector<FEIRVar*> FEIRExprJavaNewArray::GetVarUsesImpl() const {
  return exprSize->GetVarUses();
}

BaseNode *FEIRExprJavaNewArray::GenMIRNodeImpl(MIRBuilder &mirBuilder) const {
  UniqueFEIRType elemType = FEIRBuilder::CreateArrayElemType(arrayType);
  MIRType *elemMirType = elemType->GenerateMIRType(kSrcLangJava, false);
  if (!elemMirType->IsScalarType()) {
    elemMirType = GlobalTables::GetTypeTable().GetOrCreatePointerType(*elemMirType, PTY_ptr);
  }
  MIRType *jarrayType = GlobalTables::GetTypeTable().GetOrCreateJarrayType(*elemMirType);
  (void)typeID;
  MIRType *mirType = arrayType->GenerateMIRType(kSrcLangJava, false);
  MIRType *ptrType = GlobalTables::GetTypeTable().GetOrCreatePointerType(*mirType, PTY_ref);
  BaseNode *sizeNode = exprSize->GenMIRNode(mirBuilder);
  BaseNode *expr = mirBuilder.CreateExprJarrayMalloc(OP_gcmallocjarray, *ptrType, *jarrayType, sizeNode);
  CHECK_NULL_FATAL(expr);
  return expr;
}

void FEIRExprJavaNewArray::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  exprSize->RegisterDFGNodes2CheckPoint(checkPoint);
}

bool FEIRExprJavaNewArray::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  return exprSize->CalculateDefs4AllUses(checkPoint, udChain);
}

// ---------- FEIRExprJavaArrayLength ----------
FEIRExprJavaArrayLength::FEIRExprJavaArrayLength(UniqueFEIRExpr argExprArray)
    : FEIRExpr(FEIRNodeKind::kExprJavaArrayLength) {
  SetExprArray(std::move(argExprArray));
}

std::unique_ptr<FEIRExpr> FEIRExprJavaArrayLength::CloneImpl() const {
  UniqueFEIRExpr expr = std::make_unique<FEIRExprJavaArrayLength>(exprArray->Clone());
  CHECK_NULL_FATAL(expr);
  return expr;
}

std::vector<FEIRVar*> FEIRExprJavaArrayLength::GetVarUsesImpl() const {
  return exprArray->GetVarUses();
}

BaseNode *FEIRExprJavaArrayLength::GenMIRNodeImpl(MIRBuilder &mirBuilder) const {
  BaseNode *arrayNode = exprArray->GenMIRNode(mirBuilder);
  MapleVector<BaseNode*> args(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());
  args.push_back(arrayNode);
  MIRType *retType = GlobalTables::GetTypeTable().GetInt32();
  return mirBuilder.CreateExprIntrinsicop(INTRN_JAVA_ARRAY_LENGTH, OP_intrinsicop, *retType, args);
}

void FEIRExprJavaArrayLength::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  exprArray->RegisterDFGNodes2CheckPoint(checkPoint);
}

bool FEIRExprJavaArrayLength::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  return exprArray->CalculateDefs4AllUses(checkPoint, udChain);
}

// ---------- FEIRExprArrayLoad ----------
FEIRExprArrayLoad::FEIRExprArrayLoad(UniqueFEIRExpr argExprArray, UniqueFEIRExpr argExprIndex,
                                     UniqueFEIRType argTypeArray)
    : FEIRExpr(FEIRNodeKind::kExprArrayLoad),
      exprArray(std::move(argExprArray)),
      exprIndex(std::move(argExprIndex)),
      typeArray(std::move(argTypeArray)) {}

std::unique_ptr<FEIRExpr> FEIRExprArrayLoad::CloneImpl() const {
  std::unique_ptr<FEIRExpr> expr = std::make_unique<FEIRExprArrayLoad>(exprArray->Clone(), exprIndex->Clone(),
                                                                       typeArray->Clone());
  return expr;
}

BaseNode *FEIRExprArrayLoad::GenMIRNodeImpl(MIRBuilder &mirBuilder) const {
  CHECK_FATAL(exprArray->GetKind() == kExprDRead, "only support dread expr for exprArray");
  CHECK_FATAL(exprIndex->GetKind() == kExprDRead, "only support dread expr for exprIndex");
  BaseNode *addrBase = exprArray->GenMIRNode(mirBuilder);
  BaseNode *indexBn = exprIndex->GenMIRNode(mirBuilder);
  MIRType *ptrMIRArrayType = typeArray->GenerateMIRType(false);

  BaseNode *arrayExpr = mirBuilder.CreateExprArray(*ptrMIRArrayType, addrBase, indexBn);
  UniqueFEIRType typeElem = typeArray->Clone();
  (void)typeElem->ArrayDecrDim();

  MIRType *mirElemType = typeElem->GenerateMIRType(true);
  MIRType *ptrMIRElemType = GlobalTables::GetTypeTable().GetOrCreatePointerType(*mirElemType, PTY_ptr);
  BaseNode *elemBn = mirBuilder.CreateExprIread(*mirElemType, *ptrMIRElemType, 0, arrayExpr);
  return elemBn;
}

std::vector<FEIRVar*> FEIRExprArrayLoad::GetVarUsesImpl() const {
  std::vector<FEIRVar*> ans;
  for (FEIRVar *var : exprArray->GetVarUses()) {
    ans.push_back(var);
  }
  for (FEIRVar *var : exprIndex->GetVarUses()) {
    ans.push_back(var);
  }
  return ans;
}

void FEIRExprArrayLoad::RegisterDFGNodes2CheckPointImpl(FEIRStmtCheckPoint &checkPoint) {
  exprArray->RegisterDFGNodes2CheckPoint(checkPoint);
  exprIndex->RegisterDFGNodes2CheckPoint(checkPoint);
}

bool FEIRExprArrayLoad::CalculateDefs4AllUsesImpl(FEIRStmtCheckPoint &checkPoint, FEIRUseDefChain &udChain) {
  bool success = true;
  success = success && exprArray->CalculateDefs4AllUses(checkPoint, udChain);
  success = success && exprIndex->CalculateDefs4AllUses(checkPoint, udChain);
  return success;
}

// ---------- FEIRStmtPesudoLabel ----------
FEIRStmtPesudoLabel::FEIRStmtPesudoLabel(uint32 argLabelIdx)
    : FEIRStmt(kStmtPesudoLabel),
      labelIdx(argLabelIdx),
      mirLabelIdx(0) {}

std::list<StmtNode*> FEIRStmtPesudoLabel::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  StmtNode *stmtLabel = mirBuilder.CreateStmtLabel(mirLabelIdx);
  ans.push_back(stmtLabel);
  return ans;
}

void FEIRStmtPesudoLabel::GenerateLabelIdx(MIRBuilder &mirBuilder) {
  std::stringstream ss;
  ss << "label" << MPLFEEnv::GetInstance().GetGlobalLabelIdx();
  MPLFEEnv::GetInstance().IncrGlobalLabelIdx();
  mirLabelIdx = mirBuilder.GetOrCreateMIRLabel(ss.str());
}

std::string FEIRStmtPesudoLabel::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  return ss.str();
}

// ---------- FEIRStmtPesudoLabel2 ----------
LabelIdx FEIRStmtPesudoLabel2::GenMirLabelIdx(MIRBuilder &mirBuilder, uint32 qIdx0, uint32 qIdx1) {
  std::string label = "L" + std::to_string(qIdx0) + "_" + std::to_string(qIdx1);
  return mirBuilder.GetOrCreateMIRLabel(label);
}

std::pair<uint32, uint32> FEIRStmtPesudoLabel2::GetLabelIdx() const {
  return std::make_pair(labelIdxOuter, labelIdxInner);
}

std::list<StmtNode*> FEIRStmtPesudoLabel2::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  StmtNode *stmtLabel = mirBuilder.CreateStmtLabel(GenMirLabelIdx(mirBuilder, labelIdxOuter, labelIdxInner));
  ans.push_back(stmtLabel);
  return ans;
}

// ---------- FEIRStmtPesudoLOC ----------
FEIRStmtPesudoLOC::FEIRStmtPesudoLOC(uint32 argSrcFileIdx, uint32 argLineNumber)
    : FEIRStmt(kStmtPesudoLOC),
      srcFileIdx(argSrcFileIdx),
      lineNumber(argLineNumber) {
  isAuxPre = true;
}

std::list<StmtNode*> FEIRStmtPesudoLOC::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  return std::list<StmtNode*>();
}

std::string FEIRStmtPesudoLOC::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  return ss.str();
}

// ---------- FEIRStmtPesudoJavaTry ----------
FEIRStmtPesudoJavaTry::FEIRStmtPesudoJavaTry()
    : FEIRStmt(kStmtPesudoJavaTry) {}

std::list<StmtNode*> FEIRStmtPesudoJavaTry::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  MapleVector<LabelIdx> vec(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());
  for (FEIRStmtPesudoLabel *stmtLabel : catchTargets) {
    vec.push_back(stmtLabel->GetMIRLabelIdx());
  }
  StmtNode *stmtTry = mirBuilder.CreateStmtTry(vec);
  ans.push_back(stmtTry);
  return ans;
}

std::string FEIRStmtPesudoJavaTry::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  return ss.str();
}

// ---------- FEIRStmtPesudoJavaTry2 ----------
FEIRStmtPesudoJavaTry2::FEIRStmtPesudoJavaTry2(uint32 outerIdxIn)
    : FEIRStmt(kStmtPesudoJavaTry), outerIdx(outerIdxIn) {}

std::list<StmtNode*> FEIRStmtPesudoJavaTry2::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  MapleVector<LabelIdx> vec(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());
  for (uint32 target : catchLabelIdxVec) {
    vec.push_back(FEIRStmtPesudoLabel2::GenMirLabelIdx(mirBuilder, outerIdx, target));
  }
  StmtNode *stmtTry = mirBuilder.CreateStmtTry(vec);
  ans.push_back(stmtTry);
  return ans;
}

std::string FEIRStmtPesudoJavaTry2::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  return ss.str();
}

// ---------- FEIRStmtPesudoEndTry ----------
FEIRStmtPesudoEndTry::FEIRStmtPesudoEndTry()
    : FEIRStmt(kStmtPesudoEndTry) {
  isAuxPost = true;
}

std::list<StmtNode*> FEIRStmtPesudoEndTry::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  MemPool *mp = mirBuilder.GetCurrentFuncCodeMp();
  ASSERT(mp != nullptr, "mempool is nullptr");
  StmtNode *stmt = mp->New<StmtNode>(OP_endtry);
  ans.push_back(stmt);
  return ans;
}

std::string FEIRStmtPesudoEndTry::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  return ss.str();
}

// ---------- FEIRStmtPesudoCatch ----------
FEIRStmtPesudoCatch::FEIRStmtPesudoCatch(uint32 argLabelIdx)
    : FEIRStmtPesudoLabel(argLabelIdx) {}

std::list<StmtNode*> FEIRStmtPesudoCatch::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  StmtNode *stmtLabel = mirBuilder.CreateStmtLabel(mirLabelIdx);
  ans.push_back(stmtLabel);
  MapleVector<TyIdx> vec(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());
  for (const UniqueFEIRType &type : catchTypes) {
    MIRType *mirType = type->GenerateMIRType(kSrcLangJava, true);
    vec.push_back(mirType->GetTypeIndex());
  }
  StmtNode *stmtCatch = mirBuilder.CreateStmtCatch(vec);
  ans.push_back(stmtCatch);
  return ans;
}

void FEIRStmtPesudoCatch::AddCatchTypeNameIdx(GStrIdx typeNameIdx) {
  UniqueFEIRType type = std::make_unique<FEIRTypeDefault>(PTY_ref, typeNameIdx);
  catchTypes.push_back(std::move(type));
}

std::string FEIRStmtPesudoCatch::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  return ss.str();
}

// ---------- FEIRStmtPesudoCatch2 ----------
FEIRStmtPesudoCatch2::FEIRStmtPesudoCatch2(uint32 qIdx0, uint32 qIdx1)
    : FEIRStmtPesudoLabel2(qIdx0, qIdx1) {}

std::list<StmtNode*> FEIRStmtPesudoCatch2::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  StmtNode *stmtLabel = mirBuilder.CreateStmtLabel(
      FEIRStmtPesudoLabel2::GenMirLabelIdx(mirBuilder, GetLabelIdx().first, GetLabelIdx().second));
  ans.push_back(stmtLabel);
  MapleVector<TyIdx> vec(mirBuilder.GetCurrentFuncCodeMpAllocator()->Adapter());
  for (const UniqueFEIRType &type : catchTypes) {
    MIRType *mirType = type->GenerateMIRType(kSrcLangJava, true);
    vec.push_back(mirType->GetTypeIndex());
  }
  StmtNode *stmtCatch = mirBuilder.CreateStmtCatch(vec);
  ans.push_back(stmtCatch);
  return ans;
}

void FEIRStmtPesudoCatch2::AddCatchTypeNameIdx(GStrIdx typeNameIdx) {
  UniqueFEIRType type;
  if (typeNameIdx == FEUtils::GetVoidIdx()) {
    type = std::make_unique<FEIRTypePointer>(std::make_unique<FEIRTypeDefault>(PTY_void, typeNameIdx));
  } else {
    type = std::make_unique<FEIRTypeDefault>(PTY_ref, typeNameIdx);
  }
  catchTypes.push_back(std::move(type));
}

std::string FEIRStmtPesudoCatch2::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  return ss.str();
}

// ---------- FEIRStmtPesudoComment ----------
FEIRStmtPesudoComment::FEIRStmtPesudoComment(FEIRNodeKind argKind)
    : FEIRStmt(argKind) {
  isAuxPre = true;
}

FEIRStmtPesudoComment::FEIRStmtPesudoComment(const std::string &argContent)
    : FEIRStmt(kStmtPesudoComment),
      content(argContent) {
  isAuxPre = true;
}

std::list<StmtNode*> FEIRStmtPesudoComment::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  StmtNode *stmt = mirBuilder.CreateStmtComment(content);
  ans.push_back(stmt);
  return ans;
}

std::string FEIRStmtPesudoComment::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  return ss.str();
}

// ---------- FEIRStmtPesudoCommentForInst ----------
FEIRStmtPesudoCommentForInst::FEIRStmtPesudoCommentForInst()
    : FEIRStmtPesudoComment(kStmtPesudoCommentForInst) {
  isAuxPre = true;
}

std::list<StmtNode*> FEIRStmtPesudoCommentForInst::GenMIRStmtsImpl(MIRBuilder &mirBuilder) const {
  std::list<StmtNode*> ans;
  return ans;
}

std::string FEIRStmtPesudoCommentForInst::DumpDotStringImpl() const {
  std::stringstream ss;
  ss << "<stmt" << id << "> " << id << ": " << GetFEIRNodeKindDescription(kind);
  return ss.str();
}
}  // namespace maple
